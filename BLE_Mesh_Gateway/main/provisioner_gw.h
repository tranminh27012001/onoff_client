#ifndef _PROVI_GW_
#define _PROVI_GW_

#define LED_OFF             0x0
#define LED_ON              0x1

#define CID_ESP             0x02E5

#define PROV_OWN_ADDR       0x0001

#define MSG_SEND_TTL        3
#define MSG_SEND_REL        true
#define MSG_TIMEOUT         0
#define MSG_ROLE            ROLE_PROVISIONER

#define COMP_DATA_PAGE_0    0x00

#define APP_KEY_IDX         0x0000
#define APP_KEY_OCTET       0x12



typedef struct {
    uint8_t  uuid[16];
    uint8_t  elem_num;
    uint16_t unicast;
    uint8_t state[4];  // present or target state of once addr
    uint8_t cntTimeout[4];   // 
    uint8_t  onoff;
    char Device_Id[20] ;
} esp_ble_mesh_node_info_t;

 struct esp_ble_mesh_key {
    uint16_t net_idx;
    uint16_t app_idx;
    uint8_t  app_key[16];
} prov_key;

typedef struct 
{
   uint16_t addr ;
   char  Device_Id[20] ;
   uint8_t element_count;
}model_Send_note_infor_t;

typedef struct 
{
    uint8_t state;
    uint16_t addr;
}dataUpdateState_t;


esp_err_t ble_mesh_init(void) ;
void taskinit();
#endif