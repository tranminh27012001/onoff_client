#ifndef DATE_TIME_H
#define DATE_TIME_H
#include <stdio.h>
#include <stdbool.h>


#define BIT_RECV_FULL_MESS (1<<0)   // bit of event messHttp_recved

#define MAX_NUM_CMD 30
#define MAX_NUM_SCHEDULE 20       //for each device (gateway or device child)


typedef struct SchItem
{
    // time_t time;
    char deviceId[30];
    char cmds[80];
} SchItem;  //moi sch se co 1 thoi diem, tai 1 thoi diem co the co nhieu cmds

// typedef struct 
// {   
//     uint8_t deviceIndex;
//     char deviceId[30];
//     uint8_t schNum; // tổng sch của một thieeys bị
//     bool needUpdateSchedule;
//     SchItem schList[MAX_NUM_SCHEDULE];
// } SchDevice_t;


typedef struct 
{
    time_t time;
    uint8_t cmdNums; // số cmd trong một thời điểm 
    SchItem schList[MAX_NUM_CMD];
}SchDevice_t;

typedef struct 
{
    uint8_t limit;
    uint8_t skip;
}getSchedule_param_t;

void initDateTime();
void scheduleData_cb(char *scheduleStr);


#endif