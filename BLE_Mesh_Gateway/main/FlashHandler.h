/**
 ******************************************************************************
 * @file    FlashHandler.h
 * @author  Makipos Co.,LTD.
 * @date    Nov 29, 2019
 ******************************************************************************/


#ifndef __FLASH_HANDLER_H
#define __FLASH_HANDLER_H

/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/

#define NODE_STORE "NODE_STORE"


/* Exported functions ------------------------------------------------------- */
bool FlashHandler_getData(char* nameSpace,char* key,void* dataStore);
bool FlashHandler_setData(char *nameSpace, char *key, void *dataStore, size_t dataSize);

#endif /* __FLASH_HANDLER_H */
