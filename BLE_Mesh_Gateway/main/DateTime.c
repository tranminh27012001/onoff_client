#include "DateTime.h"
#include <string.h>
#include <time.h>
#include <sys/time.h>
#include "lwip/err.h"
// #include "apps/sntp/sntp.h"
#include "lwip/apps/sntp.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "freertos/event_groups.h"

#include "esp_vfs.h"
#include "esp_vfs_fat.h"
#include "cJSON.h"
#include "sntp.h"

#include "Uart_handle.h"
#include "MKP_Uart_MQTT.h"

/*******************************************************************************
 * Definitions
 ******************************************************************************/
#ifndef DISABLE_LOG_ALL
#define DATE_TIME_LOG_INFO_ON
#endif

#ifdef DATE_TIME_LOG_INFO_ON
#define log_info(format, ...) ESP_LOGI("DateTime", format, ##__VA_ARGS__)
#define log_err(format, ...) ESP_LOGE("DateTime", format, ##__VA_ARGS__)
#else
#define log_info(format, ...)
#define log_err(format, ...)
#endif



static const char *TAG = "DateTime";

 /*******************************************************************************
 * Variables
 ******************************************************************************/
SchDevice_t SchDevices[MAX_NUM_SCHEDULE] ;
uint8_t schNumCur = 0;
EventGroupHandle_t messHttpRecved_Event;
extern char *MessHttpRecv ;
extern char topic_filter[5][30] ; 

uint8_t lastDayUpdate = 8;
time_t timeStartSystem ;
bool needUpdateScheduleAll = false;
uint32_t lastTimeGetScheduleAll = 0;
uint8_t g_GettingSchedule = 0;
uint8_t Sche_NumIndex =0;
uint64_t g_timeSync=0;
bool g_mustSynTime=false;
bool readytoDoSchedule = false;
/*******************************************************************************
 * Prototypes
 ******************************************************************************/
//
static void initialize_sntp(void);
static void obtain_time(void);
static void setTimeZone();
void scheduleData_cb(char *scheduleStr);
/*******************************************************************************
 * Code
 ******************************************************************************/
static void initialize_sntp(void)
{
    log_info("Initializing SNTP");
    sntp_setoperatingmode(SNTP_OPMODE_POLL); // nó sẽ thiết lập chế độ hoạt động của SNTP trên thiết bị là chế độ "polling", có nghĩa là SNTP sẽ gửi các yêu cầu đồng bộ thời gian định kỳ cho máy chủ thời gian trên mạng
    // sntp_setservername(0, "pool.ntp.org");
    sntp_setservername(0, "time.google.com"); // lấy time từ domain "time.google.com"
    sntp_init();
}
static void setTimeZone()
{
    time_t now;
    struct tm timeinfo;
    time(&now);

    char strftime_buf[64];
    // Set timezone e
    setenv("TZ", "UTC-07:00", 1);
    tzset();
    localtime_r(&now, &timeinfo);
    strftime(strftime_buf, sizeof(strftime_buf), "%c", &timeinfo);
    log_info("The current date/time in VN is: %s", strftime_buf);
}

void scheduleData_cb(char *scheduleStr)
{   
    log_info("schedule data callback: %s",scheduleStr);
    cJSON *msgObject = cJSON_Parse(scheduleStr);
    if (msgObject == NULL)
    {
        log_err("object is null");
        const char *error_ptr = cJSON_GetErrorPtr();
        if (error_ptr != NULL)
        {
            fprintf(stderr, "Error before: %s\n", error_ptr);
        }
        return;
    }
    uint8_t total = cJSON_GetObjectItem(msgObject, "total")->valueint;
    uint8_t limit = cJSON_GetObjectItem(msgObject, "limit")->valueint;
    uint8_t skip = cJSON_GetObjectItem(msgObject, "skip")->valueint;
    log_info("Total Schedule: %d", total);
    if(total == 0)
    {
        memset(SchDevices, 0, 20000);    // chỗ này đang hard codeeeee
        log_info("CHECK: schNumCur %d",schNumCur);
    }
    else
    {       
        if(skip == 0)
        {
            memset(SchDevices, 0, 20000); 
            Sche_NumIndex = 0;
        }
        else Sche_NumIndex = skip ;
        cJSON *scheduleData = cJSON_GetObjectItem(msgObject, "data");
        uint8_t numSch = cJSON_GetArraySize(scheduleData);
        log_info("num Schedule in once call api enable: %d", numSch);
        for (uint8_t i = 0; i < numSch; i++)   // đọc từng phần tử mảng data
        {   
            Sche_NumIndex = i + skip;
            cJSON *schItem = cJSON_GetArrayItem(scheduleData, i);
            if (cJSON_GetObjectItem(schItem, "periodEnable")->type == cJSON_True)
            {
            }
            else if (cJSON_GetObjectItem(schItem, "periodEnable")->type == cJSON_False)
            {   
                SchDevices[Sche_NumIndex].time = cJSON_GetObjectItem(schItem, "time")->valueint;
                cJSON *cmdList = cJSON_GetObjectItem(schItem, "commands"); // đọc comands
                SchDevices[Sche_NumIndex].cmdNums = cJSON_GetArraySize(cmdList);               
                for(uint8_t numCmdidx =0; numCmdidx<SchDevices[Sche_NumIndex].cmdNums; numCmdidx++)
                {   
                    cJSON *cmd = cJSON_GetArrayItem(cmdList, numCmdidx);
                    char *produc_id = cJSON_GetObjectItem(cmd, "productId")->valuestring;
                    // printf("\nproduc_id :%s\n", produc_id);
                    strcpy(SchDevices[Sche_NumIndex].schList[numCmdidx].deviceId, produc_id);
                    char *cmdStr = cJSON_GetObjectItem(cmd, "commandString")->valuestring;
                    strcpy(SchDevices[Sche_NumIndex].schList[numCmdidx].cmds, cmdStr);
                    // printf("\ncommandString :%s\n", cmdStr);
                }
            }
        }
        Sche_NumIndex++;
        schNumCur = Sche_NumIndex;
    }
    if(total == limit + skip)
    {
        readytoDoSchedule = true ;
    }
    else 
    {       
        getSchedule_param_t getSchedule_param;
        if(total == (limit + skip +1)) getSchedule_param.limit = 1;
        else  getSchedule_param.limit = 2;
        getSchedule_param.skip= skip + limit;
        Uart_send_GetblobScheParam(&getSchedule_param, sizeof(getSchedule_param_t));
        readytoDoSchedule = false;

    }
    cJSON_Delete(msgObject);
    // lastDayUpdate = weekDay();  
}

static void checkAndDoSchedule()
{
    printf("schNumCur:%d\n", schNumCur);
    for(uint8_t index = 0; index < schNumCur; index++)        //check trong nhuwng device co sch
    {
        time_t now;
        time(&now);
        if ((now >=SchDevices[index].time) && ((now - SchDevices[index].time) < 60))  //den thoi diem cua device do
        {
            for (uint8_t cmdCnt = 0; cmdCnt < SchDevices[index].cmdNums; cmdCnt++)     //chay tat ca cmd tai thoi diem nay cua device do
            {
                cJSON *schData = cJSON_Parse(SchDevices[index].schList[cmdCnt].cmds);  
                strcpy(topic_filter[PRODUCT_ID],SchDevices[index].schList[cmdCnt].deviceId);
                strcpy(topic_filter[EVENT_TYPE],"CP");
                strcpy(topic_filter[PROPERTY_CODE] ,cJSON_GetObjectItem(schData,"code")->valuestring);
                strcpy(topic_filter[LOCAL_ID] ,cJSON_GetObjectItem(schData,"localId")->valuestring);
                cJSON* dataOfCmd = cJSON_GetObjectItem(schData,"data"); 
                char *data_sch =NULL; 
                if(dataOfCmd->type == cJSON_Number)
                {
                    data_sch = (char*)malloc(80);
                    if(data_sch)
                    {
                        sprintf(data_sch,"{\"d\":%d, \"r\":\"SCHEDULE\"}",cJSON_GetObjectItem(schData,"data")->valueint);
                    }
                }
                else if(dataOfCmd->type == cJSON_Array)
                {
                }
                if(data_sch)
                {
                    log_info("data send to PROTO_processCmd: %s", data_sch);
                    log_info("productId_sch: %s\n",topic_filter[PRODUCT_ID]);
                    log_info( "EventType: %s\n",topic_filter[EVENT_TYPE]);
                    log_info("PropertyCode: %s\n",topic_filter[PROPERTY_CODE]);
                    log_info("LocalId: %s\n",topic_filter[LOCAL_ID]);
                    //PROTO_processCmd(schList[i].cmds[cmdCnt]);
                    PROTO_processCmd(data_sch);      
                }
                free(data_sch);
                cJSON_Delete(schData);    //chi do schedule 1 lan.
                printf("\n\n");
            }
        } 
        SchDevices[index].time = 0;            
    }
} 

static void dateTimeTask(void *arg)
{
    time_t now;   // typedef time_t long
    struct tm timeinfo;
    struct timeval tv = {0};
    setTimeZone();
    time(&now);
    localtime_r(&now, &timeinfo);
    timeStartSystem = now;
    while (1)
    {       
        if(g_mustSynTime)
        {
            tv.tv_sec = g_timeSync;
            tv.tv_usec = 0 ;
            settimeofday(&tv, NULL);
            setTimeZone();
            time(&now);
            localtime_r(&now, &timeinfo);     
            char strftime_buf[64];
            strftime(strftime_buf, sizeof(strftime_buf), "%c", &timeinfo); // Hàm này cho phép định dạng chuỗi ký tự theo định dạng mà người dùng mong muốn
            // log_info("The current date/time in device is: %s", strftime_buf); 
            vTaskDelay(500 / portTICK_RATE_MS);
            g_mustSynTime=false;
        }
        else
        {   
            vTaskDelay(4000 / portTICK_RATE_MS);
            // time(&now);
            // localtime_r(&now, &timeinfo);     
            // char strftime_buf[64];
            // strftime(strftime_buf, sizeof(strftime_buf), "%c", &timeinfo); // Hàm này cho phép định dạng chuỗi ký tự theo định dạng mà người dùng mong muốn
            // log_info("The current date/time in device is: %s", strftime_buf);
            if(readytoDoSchedule)     
            {
                checkAndDoSchedule();

            }
               

        }
    // printf("stack free of task dateTimeTask: %d \n", (int)uxTaskGetStackHighWaterMark(NULL));
}
}

static void scheUarthandleTask (void *arg)
{
    messHttpRecved_Event = xEventGroupCreate();
    while (1)
    {
        xEventGroupWaitBits(messHttpRecved_Event,  BIT_RECV_FULL_MESS, pdTRUE, pdFALSE, portMAX_DELAY);
        char A[3000] ={0};
        memset(A,0,3000);
        strcpy(A, MessHttpRecv);
        printf ("leng mes http : %d", strlen(A));
        scheduleData_cb(A);
        free(MessHttpRecv);
        MessHttpRecv = NULL;
    }
    
}
void initDateTime()
{
    xTaskCreate(dateTimeTask, "dateTimeTask", 15 * 1024, NULL, 5, NULL);
    xTaskCreate(scheUarthandleTask, "scheUarthandleTask", 1024 *10, NULL, 5, NULL);

}