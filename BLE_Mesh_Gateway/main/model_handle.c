#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#include "esp_log.h"
#include "esp_err.h"
#include "nvs_flash.h"
#include <sdkconfig.h>

#include "esp_ble_mesh_common_api.h"
#include "esp_ble_mesh_provisioning_api.h"
#include "esp_ble_mesh_networking_api.h"
#include "esp_ble_mesh_config_model_api.h"
#include "esp_ble_mesh_generic_model_api.h"
#include "esp_ble_mesh_defs.h"
#include "esp_ble_mesh_time_scene_model_api.h"

#include "ble_mesh_example_init.h"
// #include "ble_mesh_example_nvs.h"

#include "esp_bt.h"
#include "esp_bt_main.h"
#include "esp_bt_device.h"

#include "mesh_buf.h"
#include "custom_model_defs.h"
#include "provisioner_gw.h"
#include "MKP_Uart_MQTT.h"
#include "prov_handle.h"

/*******************************************************************************
 * Definitions
 ******************************************************************************/
#define TAG "MODEL_HANDLE"

 /*******************************************************************************
 * Variables
 ******************************************************************************/
extern esp_ble_mesh_client_t onoff_client;
extern esp_ble_mesh_client_t scene_client;
extern esp_ble_mesh_client_t custom_sensor_client ;
extern esp_ble_mesh_client_t custom_model_ctr_client ;
extern esp_ble_mesh_client_t custom_model_ack_client;
extern esp_ble_mesh_node_info_t nodes[CONFIG_BLE_MESH_MAX_PROV_NODES] ;
extern char Device_Id[20] ;
extern char PassWord [20];
uint8_t location_in_nodes = 0 ;
/*******************************************************************************
 * Prototypes
 ******************************************************************************/
static esp_err_t example_ble_mesh_send_gen_onoff_set(uint16_t addr, uint8_t state, uint8_t tid);
void Ble_mesh_Pub_state (uint16_t addr, uint8_t state) ;

/*******************************************************************************
 * Code
 ******************************************************************************/
/* HANDLE OF GENERIC ONOFF CLIENT*/
esp_err_t Send_onoff_to_Device (char *g_product_id, uint8_t local_id,uint8_t state,  uint8_t mid )
{       
    esp_err_t err = ESP_FAIL;
    uint16_t addr ;
    for(uint8_t x=0; x< CONFIG_BLE_MESH_MAX_PROV_NODES ; x++)
    {    
       if(nodes[x].unicast !=0)
       {
        if(strcmp( g_product_id , nodes[x].Device_Id)==0)
        {   
             ESP_LOGI(TAG, " 0x%04x", nodes[x].unicast);
             ESP_LOGI(TAG, "%s", nodes[x].Device_Id);
             addr = nodes[x].unicast + local_id ;
             nodes[x].onoff = state;
             nodes[x].state[local_id] = state;
             nodes[x].cntTimeout[local_id] = 2;
             err = example_ble_mesh_send_gen_onoff_set(addr, state, 10);
        }
       }
    }
    if(err == ESP_OK)
    {
        // Ble_mesh_Pub_state(addr, state);
        return ESP_OK;
    }
    return ESP_FAIL;
}
static esp_err_t example_ble_mesh_send_gen_onoff_set(uint16_t addr, uint8_t state, uint8_t tid)
{
    esp_ble_mesh_generic_client_set_state_t set = {0};
    esp_ble_mesh_client_common_param_t common = {0};
    esp_err_t err = ESP_OK;

    common.opcode = ESP_BLE_MESH_MODEL_OP_GEN_ONOFF_SET;
    common.model = onoff_client.model;
    common.ctx.net_idx = prov_key.net_idx;
    common.ctx.app_idx = prov_key.app_idx;
    common.ctx.addr = addr; /* to all nodes */
    common.ctx.send_ttl = 3;
    common.ctx.send_rel = true;
    common.msg_timeout = 0; /* 0 indicates that timeout value from menuconfig will be used */
    common.msg_role = ROLE_PROVISIONER;

    set.onoff_set.op_en = false;
    set.onoff_set.trans_time = 0;
    set.onoff_set.onoff = state;
    set.onoff_set.tid = tid;

    err = esp_ble_mesh_generic_client_set_state(&common, &set);
    if (err != ESP_OK )
    {
        ESP_LOGE(TAG, "Send Generic OnOff Set Unack failed");
        return ESP_FAIL ;
    }
    return ESP_OK;

}

static uint8_t Find_Local_Id (uint16_t addr)
{   
    uint8_t local_id;
    location_in_nodes =0 ;
     for(; location_in_nodes< ARRAY_SIZE(nodes); location_in_nodes++)
    {
        if((nodes[location_in_nodes].unicast <= addr) && ((nodes[location_in_nodes].elem_num+nodes[location_in_nodes].unicast)> addr))
        {
            break;
        }
    }
    local_id = addr-nodes[location_in_nodes].unicast;
    return local_id;
}
void Ble_mesh_Pub_state (uint16_t addr, uint8_t state)  // Function publish state to server
{   
    uint8_t Local_id = Find_Local_Id (addr);
    // Local_id+=1;
    printf("addr: 0x%04x , location in nodes :%d\n",addr, location_in_nodes);
    UART_TO_MQTT_PublishNewState(nodes[location_in_nodes].Device_Id , "W_SWITCH",LOCAL_ID_DEVICE_TO_SERVER(Local_id), state, NULL, "TOUCH", NULL);  // Tạm thời để reason là "touch"
}
void example_ble_mesh_send_gen_onoff_get(uint16_t addr)
{
    esp_ble_mesh_generic_client_get_state_t get = {0};
    esp_ble_mesh_client_common_param_t common = {0};
    esp_err_t err = ESP_OK;

    common.opcode = ESP_BLE_MESH_MODEL_OP_GEN_ONOFF_GET;
    common.model = onoff_client.model;
    common.ctx.net_idx = prov_key.net_idx;
    common.ctx.app_idx = prov_key.app_idx;
    common.ctx.addr = addr; /* to all nodes */
    common.ctx.send_ttl = 3;
    common.ctx.send_rel = false;
    common.msg_timeout = 0; /* 0 indicates that timeout value from menuconfig will be used */
    common.msg_role = ROLE_PROVISIONER;


    err = esp_ble_mesh_generic_client_get_state(&common, &get);
    if (err)
    {
        ESP_LOGE(TAG, "Send Generic OnOff Set Unack failed");
        return;
    }

}
/*HANDLE OF SCENE MODEL*/
void handle_client_scene_set_store( uint16_t scene_number_ )      // Hàm tạo scene
{
    esp_ble_mesh_client_common_param_t common={0};
    esp_ble_mesh_time_scene_client_set_state_t set={0};
    common.model = scene_client.model;
    common.opcode = ESP_BLE_MESH_MODEL_OP_SCENE_STORE;
   common.ctx.net_idx = prov_key.net_idx;
    common.ctx.app_idx = prov_key.app_idx;
    common.ctx.addr = 0xFFFF;
    common.ctx.send_ttl = 3;
    common.ctx.send_rel = false;
    common.msg_timeout = 0;
    common.msg_role = ROLE_PROVISIONER;

   
    set.scene_store.scene_number = scene_number_;
    

    esp_ble_mesh_time_scene_client_set_state(&common, &set);
     printf("done storeeee");
}

void handle_client_scene_delete( uint16_t scene_number_ )      // Hàm delete scene
{
    esp_ble_mesh_client_common_param_t common={0};
    esp_ble_mesh_time_scene_client_set_state_t set={0};
    common.model = scene_client.model;
    common.opcode = ESP_BLE_MESH_MODEL_OP_SCENE_DELETE;
   common.ctx.net_idx = prov_key.net_idx;
    common.ctx.app_idx = prov_key.app_idx;
    common.ctx.addr = 0xFFFF;
    common.ctx.send_ttl = 3;
    common.ctx.send_rel = false;
    common.msg_timeout = 0;
    common.msg_role = ROLE_PROVISIONER;
    set.scene_store.scene_number = scene_number_;
    esp_ble_mesh_time_scene_client_set_state(&common, &set);
}
void handle_client_scene_set_ReCall ( uint16_t scene_number_, uint16_t net_idx, uint16_t app_idx)               // Hàm gọi ReCall
{   
    esp_ble_mesh_client_common_param_t common= {0} ;
    esp_ble_mesh_time_scene_client_set_state_t set ={0}  ;
    common.model = scene_client.model ;
    common.opcode = ESP_BLE_MESH_MODEL_OP_SCENE_RECALL_UNACK ;
    
  common.ctx.net_idx = prov_key.net_idx;
    common.ctx.app_idx = prov_key.app_idx;
    common.ctx.addr = 0xFFFF; 
    common.ctx.send_ttl = 3;
    common.ctx.send_rel = false;
    common.msg_timeout = 0; 
    common.msg_role = ROLE_PROVISIONER;
    
    set.scene_recall.op_en = false;
    set.scene_recall.scene_number= scene_number_;
    set.scene_recall.tid = 0 ;
    
     esp_ble_mesh_time_scene_client_set_state(&common, &set);
     printf("enddddddddd handle client set scene");
} 


/*custom handle*/
 void parse_received_data(esp_ble_mesh_model_cb_param_t *recv_param, model_scene_onoff_data_t *parsed_data) 
{
    if (recv_param->client_recv_publish_msg.length < sizeof(parsed_data)) {
        ESP_LOGE(TAG, "Invalid received message lenght: %d", recv_param->client_recv_publish_msg.length);
        return;
    }
    parsed_data = (model_scene_onoff_data_t *)recv_param->client_recv_publish_msg.msg;
}

void parse_rev_data_model_ctr(esp_ble_mesh_model_cb_param_t *recv_param, model_control_data_t *parsed_data)
{
    if(recv_param->client_recv_publish_msg.length< sizeof(parsed_data)){
        ESP_LOGE(TAG, "Invalid received message lenght: %d", recv_param->client_recv_publish_msg.length);
        return;
    }
    parsed_data = (model_control_data_t *)recv_param->client_recv_publish_msg.msg;
}

esp_err_t ble_mesh_custom_elm_control_client_model_message_set(model_control_data_t * set_data, uint16_t addr) 
{
        esp_ble_mesh_msg_ctx_t ctx = {0};
        uint32_t opcode;
        esp_err_t err;

        opcode = ESP_BLE_MESH_CUSTOM_CONTROL_MODEL_OP_SET;
        
        ctx.net_idx = prov_key.net_idx;
        ctx.app_idx = prov_key.app_idx;
        // ctx.addr = ESP_BLE_MESH_ADDR_ALL_NODES;
        ctx.addr = addr;
        ctx.send_ttl = 3;
        ctx.send_rel = false;

        err = esp_ble_mesh_client_model_send_msg(custom_model_ctr_client.model, &ctx, opcode,
                sizeof(set_data), (uint8_t *) set_data, 0, false, ROLE_PROVISIONER);

        if (err != ESP_OK) {
            ESP_LOGE(TAG, "Falha ao enviar a msg custom 0x%06x, err = 0x%06x", opcode, err);
        }

        return err;
}
esp_err_t ble_mesh_custom_scene_onoff_message_set(model_scene_onoff_data_t *set_data) // fuction set state
{
        esp_ble_mesh_msg_ctx_t ctx = {0};
        uint32_t opcode;
        esp_err_t err;
        opcode = ESP_BLE_MESH_CUSTOM_SENSOR_MODEL_OP_SET;
        ctx.net_idx = prov_key.net_idx;
        ctx.app_idx = prov_key.app_idx;
        // ctx.addr = addr;
        ctx.addr = 0xFFFF;
        ctx.send_ttl = 3;
        ctx.send_rel = false;

        err = esp_ble_mesh_client_model_send_msg(custom_sensor_client.model, &ctx, opcode,
                sizeof(set_data), (uint8_t *)set_data, 0, false, ROLE_PROVISIONER);

        if (err != ESP_OK) {
            ESP_LOGE(TAG, "Falha ao enviar a msg custom 0x%06x, err = 0x%06x", opcode, err);
        }

        return err;
}
esp_err_t ble_mesh_custom_ack_state_message_set(model_ack_state_t *ack_param) // fuction confirm state
{          
        esp_ble_mesh_node_info_t *node = NULL;
        node = ble_mesh_get_node_info(ack_param->addr);
        esp_ble_mesh_msg_ctx_t ctx = {0};
        uint32_t opcode;
        esp_err_t err;
        opcode = ESP_BLE_MESH_CUSTOM_ACK_STATE_OP_SET;
        ctx.net_idx = prov_key.net_idx;
        ctx.app_idx = prov_key.app_idx;
        // ctx.addr = addr;
        ctx.addr = node->unicast;
        ctx.send_ttl = 3;
        ctx.send_rel = false;

        err = esp_ble_mesh_client_model_send_msg(custom_model_ack_client.model, &ctx, opcode,
                sizeof(model_ack_state_t), (uint8_t *)ack_param, 0, false, ROLE_PROVISIONER);

        if (err != ESP_OK) {
            ESP_LOGE(TAG, "Falha ao enviar a msg custom 0x%06x, err = 0x%06x", opcode, err);
        }

        return err;
}

void set_scene_and_elm_control(void)  // function test
{       model_control_data_t set_data_control ;
        set_data_control.element_control_adrr =0x0005;
        set_data_control.scene =0x0001 ;
        model_scene_onoff_data_t set_data_scene ;
        set_data_scene.addrr= 0x0006 ;
        set_data_scene.state=1; 
        set_data_scene.scene =0x0001;
        handle_client_scene_set_store (0x0001);
        vTaskDelay(500/portTICK_RATE_MS);
        ble_mesh_custom_elm_control_client_model_message_set(&set_data_control, 0x0005);
        vTaskDelay(500/portTICK_RATE_MS);
        ble_mesh_custom_scene_onoff_message_set(&set_data_scene);

}
// Parse Device infor
// SƯ,1234
bool Parse_device_infor (char * data_recv)
{
//   printf("data_recv : %s", data_recv);
  char *pt= strtok(data_recv , ",");
  if(pt == NULL)
  {
    // ESP_LOGW(TAG,"PT IS NULL");
    return false;
  }
  else
  {
    strcpy(Device_Id, pt);
    ESP_LOGI(TAG,"Device_Id: %s", Device_Id );
    pt = strtok(NULL , ",");
  }
  return true;
}