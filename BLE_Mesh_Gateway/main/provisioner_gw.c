#include <stdio.h>
#include <string.h>

#include "esp_log.h"
#include "nvs_flash.h"

#include "esp_ble_mesh_defs.h"
#include "esp_ble_mesh_common_api.h"
#include "esp_ble_mesh_provisioning_api.h"
#include "esp_ble_mesh_networking_api.h"
#include "esp_ble_mesh_config_model_api.h"
#include "esp_ble_mesh_generic_model_api.h"
#include "esp_ble_mesh_time_scene_model_api.h"
#include "esp_ble_mesh_local_data_operation_api.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "freertos/event_groups.h"

#include "ble_mesh_example_init.h"
// #include "ble_mesh_example_nvs.h"
#include "mesh_buf.h" /*add test*/
#include "custom_model_defs.h"
#include "provisioner_gw.h"
#include "model_hanlde.h"
#include "prov_handle.h"
#include "Uart_handle.h"
#include "FlashHandler.h"

/*******************************************************************************
 * Definitions
 ******************************************************************************/
#define TAG "PROVI_GW"

 /*******************************************************************************
 * Variables
 ******************************************************************************/
uint8_t dev_uuid[16];
char Device_Id[20] = {0};
char PassWord[20] = {0};
esp_ble_mesh_node_info_t nodes[CONFIG_BLE_MESH_MAX_PROV_NODES] = {
    [0 ...(CONFIG_BLE_MESH_MAX_PROV_NODES - 1)] = {
        .unicast = ESP_BLE_MESH_ADDR_UNASSIGNED,
        .elem_num = 0,
        .onoff = LED_OFF,
        // .state = {0};
        // .cntTimeout={0};
        // .Device_Id = NULL,
    }};

/*******************************************************************************
 * Prototypes
 ******************************************************************************/

/*******************************************************************************
 * Code
 ******************************************************************************/
/* config server */
static esp_ble_mesh_cfg_srv_t config_server = {
    .relay = ESP_BLE_MESH_RELAY_ENABLED,
    .beacon = ESP_BLE_MESH_BEACON_ENABLED,
#if defined(CONFIG_BLE_MESH_FRIEND)
    .friend_state = ESP_BLE_MESH_FRIEND_ENABLED,
#else
    .friend_state = ESP_BLE_MESH_FRIEND_NOT_SUPPORTED,
#endif
#if defined(CONFIG_BLE_MESH_GATT_PROXY_SERVER)
    .gatt_proxy = ESP_BLE_MESH_GATT_PROXY_ENABLED,
#else
    .gatt_proxy = ESP_BLE_MESH_GATT_PROXY_NOT_SUPPORTED,
#endif
    .default_ttl = 7,
    /* 3 transmissions with 20ms interval */
    .net_transmit = ESP_BLE_MESH_TRANSMIT(2, 20),
    .relay_retransmit = ESP_BLE_MESH_TRANSMIT(2, 20),
};

/*  config client */
esp_ble_mesh_client_t config_client;
/* -----set up onoff client model --------------*/
esp_ble_mesh_client_t onoff_client;

/*------set up scene client--------------------*/
esp_ble_mesh_client_t scene_client;

ESP_BLE_MESH_MODEL_PUB_DEFINE(scene_cli_pub, 2 + 1, ROLE_PROVISIONER);

/*------set up custom set scene onoff model--------------*/

const esp_ble_mesh_client_op_pair_t custom_model_op_pair[] = {
    {ESP_BLE_MESH_CUSTOM_SENSOR_MODEL_OP_GET, ESP_BLE_MESH_CUSTOM_SENSOR_MODEL_OP_STATUS},
};

esp_ble_mesh_model_op_t custom_sensor_op[] = {
    ESP_BLE_MESH_MODEL_OP(ESP_BLE_MESH_CUSTOM_SENSOR_MODEL_OP_STATUS, 2),
    ESP_BLE_MESH_MODEL_OP_END,
};

esp_ble_mesh_client_t custom_sensor_client = {
    .op_pair_size = ARRAY_SIZE(custom_model_op_pair),
    .op_pair = custom_model_op_pair,
};

/*-----------set up custom control model---------------*/
const esp_ble_mesh_client_op_pair_t custom_model_crt_op_pair[] = {
    {ESP_BLE_MESH_CUSTOM_CONTROL_MODEL_OP_GET, ESP_BLE_MESH_CUSTOM_CONTROL_MODEL_OP_STATUS},
};

esp_ble_mesh_model_op_t custom_model_ctr_op[] = {
    ESP_BLE_MESH_MODEL_OP(ESP_BLE_MESH_CUSTOM_CONTROL_MODEL_OP_STATUS, 2),
    ESP_BLE_MESH_MODEL_OP_END,
};

esp_ble_mesh_client_t custom_model_ctr_client = {
    .op_pair_size = ARRAY_SIZE(custom_model_crt_op_pair),
    .op_pair = custom_model_crt_op_pair,
};
/*-----------set up ACK STATE model---------------*/
const esp_ble_mesh_client_op_pair_t custom_model_ack_op_pair[] = {
    {ESP_BLE_MESH_CUSTOM_ACK_STATE_OP_GET, ESP_BLE_MESH_CUSTOM_ACK_STATE_OP_STATUS},
};

esp_ble_mesh_model_op_t custom_model_ack_op[] = {
    ESP_BLE_MESH_MODEL_OP(ESP_BLE_MESH_CUSTOM_ACK_STATE_OP_STATUS, 1),
    ESP_BLE_MESH_MODEL_OP_END,
};

esp_ble_mesh_client_t custom_model_ack_client = {
    .op_pair_size = ARRAY_SIZE(custom_model_ack_op_pair),
    .op_pair = custom_model_ack_op_pair,
};
/*----------SET UP MODEL RECV INFOR FACTORY-----------*/
esp_ble_mesh_model_op_t recv_infor_factory_op[] = {
    ESP_BLE_MESH_MODEL_OP(ESP_BLE_MESH_CUSTOM_INFOR_FACTORY_OP_GET, 0),
    ESP_BLE_MESH_MODEL_OP(ESP_BLE_MESH_CUSTOM_INFOR_FACTORY_OP_SET, 4),
    ESP_BLE_MESH_MODEL_OP_END,
};
model_recv_infor_factory_t recv_infor_factory_data;

esp_ble_mesh_model_t custom_models[] = {
    ESP_BLE_MESH_VENDOR_MODEL(CID_ESP, ESP_BLE_MESH_CUSTOM_SENSOR_MODEL_ID_CLIENT,
                              custom_sensor_op, NULL, &custom_sensor_client),
    ESP_BLE_MESH_VENDOR_MODEL(CID_ESP, ESP_BLE_MESH_CUSTOM_CONTROL_MODEL_ID_CLIENT,
                              custom_model_ctr_op, NULL, &custom_model_ctr_client),
    ESP_BLE_MESH_VENDOR_MODEL(CID_ESP, ESP_BLE_MESH_CUSTOM_ACK_MODEL_ID_CLIENT,
    custom_model_ack_op, NULL, &custom_model_ack_client),
    ESP_BLE_MESH_VENDOR_MODEL(CID_ESP, ESP_BLE_MESH_CUSTOM_INFOR_FACTORY_ID_SERVER,
                              recv_infor_factory_op, NULL, &recv_infor_factory_data),
    

};
/* link element */
esp_ble_mesh_model_t root_models[] = {
    ESP_BLE_MESH_MODEL_CFG_SRV(&config_server),
    ESP_BLE_MESH_MODEL_CFG_CLI(&config_client),
    ESP_BLE_MESH_MODEL_GEN_ONOFF_CLI(NULL, &onoff_client),
    ESP_BLE_MESH_MODEL_SCENE_CLI(&scene_cli_pub, &scene_client),
};

esp_ble_mesh_elem_t elements[] = {
    ESP_BLE_MESH_ELEMENT(0, root_models, custom_models),
};

esp_ble_mesh_comp_t composition = {
    .cid = CID_ESP,
    .elements = elements,
    .element_count = ARRAY_SIZE(elements),
};

esp_ble_mesh_prov_t provision = {
    .prov_uuid = dev_uuid,
    .prov_unicast_addr = PROV_OWN_ADDR,
    .prov_start_address = 0x0005,
    .prov_attention = 0x00,
    .prov_algorithm = 0x00,
    .prov_pub_key_oob = 0x00,
    .prov_static_oob_val = NULL,
    .prov_static_oob_len = 0x00,
    .flags = 0x00,
    .iv_index = 0x00,
};

/************FUNCTION CALLBACK *****************/

static void example_ble_mesh_provisioning_cb(esp_ble_mesh_prov_cb_event_t event,
                                             esp_ble_mesh_prov_cb_param_t *param)
{
    switch (event)
    {
    case ESP_BLE_MESH_PROVISIONER_PROV_ENABLE_COMP_EVT:
        ESP_LOGI(TAG, "ESP_BLE_MESH_PROVISIONER_PROV_ENABLE_COMP_EVT, err_code %d", param->provisioner_prov_enable_comp.err_code);
        break;
    case ESP_BLE_MESH_PROVISIONER_PROV_DISABLE_COMP_EVT:
        ESP_LOGI(TAG, "ESP_BLE_MESH_PROVISIONER_PROV_DISABLE_COMP_EVT, err_code %d", param->provisioner_prov_disable_comp.err_code);
        break;
    case ESP_BLE_MESH_PROVISIONER_RECV_UNPROV_ADV_PKT_EVT:
        ESP_LOGI(TAG, "ESP_BLE_MESH_PROVISIONER_RECV_UNPROV_ADV_PKT_EVT");
        recv_unprov_adv_pkt(param->provisioner_recv_unprov_adv_pkt.dev_uuid, param->provisioner_recv_unprov_adv_pkt.addr,
                            param->provisioner_recv_unprov_adv_pkt.addr_type, param->provisioner_recv_unprov_adv_pkt.oob_info,
                            param->provisioner_recv_unprov_adv_pkt.adv_type, param->provisioner_recv_unprov_adv_pkt.bearer);
        break;
    case ESP_BLE_MESH_PROVISIONER_PROV_LINK_OPEN_EVT:
        prov_link_open(param->provisioner_prov_link_open.bearer);
        break;
    case ESP_BLE_MESH_PROVISIONER_PROV_LINK_CLOSE_EVT:
        prov_link_close(param->provisioner_prov_link_close.bearer, param->provisioner_prov_link_close.reason);
        break;
    case ESP_BLE_MESH_PROVISIONER_PROV_COMPLETE_EVT:
        prov_complete(param->provisioner_prov_complete.node_idx, param->provisioner_prov_complete.device_uuid,
                      param->provisioner_prov_complete.unicast_addr, param->provisioner_prov_complete.element_num,
                      param->provisioner_prov_complete.netkey_idx);
        break;
    case ESP_BLE_MESH_PROVISIONER_ADD_UNPROV_DEV_COMP_EVT:
        ESP_LOGI(TAG, "ESP_BLE_MESH_PROVISIONER_ADD_UNPROV_DEV_COMP_EVT, err_code %d", param->provisioner_add_unprov_dev_comp.err_code);
        break;
    case ESP_BLE_MESH_PROVISIONER_SET_DEV_UUID_MATCH_COMP_EVT:
        ESP_LOGI(TAG, "ESP_BLE_MESH_PROVISIONER_SET_DEV_UUID_MATCH_COMP_EVT, err_code %d", param->provisioner_set_dev_uuid_match_comp.err_code);
        break;
    case ESP_BLE_MESH_PROVISIONER_SET_NODE_NAME_COMP_EVT:
    {
        ESP_LOGI(TAG, "ESP_BLE_MESH_PROVISIONER_SET_NODE_NAME_COMP_EVT, err_code %d", param->provisioner_set_node_name_comp.err_code);
        if (param->provisioner_set_node_name_comp.err_code == ESP_OK)
        {
            const char *name = NULL;
            name = esp_ble_mesh_provisioner_get_node_name(param->provisioner_set_node_name_comp.node_index);
            if (!name)
            {
                ESP_LOGE(TAG, "Get node name failed");
                return;
            }
            ESP_LOGI(TAG, "Node %d name is: %s", param->provisioner_set_node_name_comp.node_index, name);
        }
        break;
    }
    case ESP_BLE_MESH_PROVISIONER_ADD_LOCAL_APP_KEY_COMP_EVT:
    {
        ESP_LOGI(TAG, "ESP_BLE_MESH_PROVISIONER_ADD_LOCAL_APP_KEY_COMP_EVT, err_code %d", param->provisioner_add_app_key_comp.err_code);
        if (param->provisioner_add_app_key_comp.err_code == ESP_OK)
        {
            esp_err_t err = 0;
            prov_key.app_idx = param->provisioner_add_app_key_comp.app_idx;
            err = esp_ble_mesh_provisioner_bind_app_key_to_local_model(PROV_OWN_ADDR, prov_key.app_idx,
                                                                       ESP_BLE_MESH_MODEL_ID_GEN_ONOFF_CLI, ESP_BLE_MESH_CID_NVAL);
            if (err != ESP_OK)
            {
                ESP_LOGE(TAG, "Provisioner bind local model appkey failed");
                return;
            }
            err = esp_ble_mesh_provisioner_bind_app_key_to_local_model(PROV_OWN_ADDR, prov_key.app_idx,
                                                                       ESP_BLE_MESH_MODEL_ID_SCENE_CLI, ESP_BLE_MESH_CID_NVAL);
            if (err != ESP_OK)
            {
                ESP_LOGE(TAG, "Provisioner bind local model appkey failed");
                return;
            }
            err = esp_ble_mesh_provisioner_bind_app_key_to_local_model(PROV_OWN_ADDR, prov_key.app_idx,
                                                                       ESP_BLE_MESH_CUSTOM_SENSOR_MODEL_ID_CLIENT, CID_ESP);
            if (err != ESP_OK)
            {
                ESP_LOGE(TAG, "Provisioner bind local model appkey failed");
                return;
            }
            err = esp_ble_mesh_provisioner_bind_app_key_to_local_model(PROV_OWN_ADDR, prov_key.app_idx,
                                                                       ESP_BLE_MESH_CUSTOM_CONTROL_MODEL_ID_CLIENT, CID_ESP);
            if (err != ESP_OK)
            {
                ESP_LOGE(TAG, "Provisioner bind local model appkey failed");
                return;
            }
            err = esp_ble_mesh_provisioner_bind_app_key_to_local_model(PROV_OWN_ADDR, prov_key.app_idx,
                                                                       ESP_BLE_MESH_CUSTOM_INFOR_FACTORY_ID_SERVER, CID_ESP);
            if (err != ESP_OK)
            {
                ESP_LOGE(TAG, "Provisioner bind local model appkey failed");
                return;
            }
            err = esp_ble_mesh_provisioner_bind_app_key_to_local_model(PROV_OWN_ADDR, prov_key.app_idx,
                                                                       ESP_BLE_MESH_CUSTOM_ACK_MODEL_ID_CLIENT, CID_ESP);
            if (err != ESP_OK)
            {
                ESP_LOGE(TAG, "Provisioner bind local model appkey failed");
                return;
            }
        }
        break;
    }
    case ESP_BLE_MESH_PROVISIONER_BIND_APP_KEY_TO_MODEL_COMP_EVT:
        ESP_LOGI(TAG, "ESP_BLE_MESH_PROVISIONER_BIND_APP_KEY_TO_MODEL_COMP_EVT, err_code %d", param->provisioner_bind_app_key_to_model_comp.err_code);
        esp_err_t err = esp_ble_mesh_model_subscribe_group_addr(PROV_OWN_ADDR, ESP_BLE_MESH_CID_NVAL, ESP_BLE_MESH_MODEL_ID_GEN_ONOFF_CLI, ESP_BLE_MESH_GROUP_PUB_ADDR);

        if (err != ESP_OK)
        {
            ESP_LOGE(TAG, "Failed to subscribe to group 0x%04x", ESP_BLE_MESH_GROUP_PUB_ADDR);
        }
        break;
    case ESP_BLE_MESH_MODEL_SUBSCRIBE_GROUP_ADDR_COMP_EVT:
        ESP_LOGI(TAG, "ESP_BLE_MESH_MODEL_SUBSCRIBE_GROUP_ADDR_COMP_EVT, err_code %d", param->model_sub_group_addr_comp.err_code);
        break;
    default:
        break;
    }

    return;
}

static void example_ble_mesh_config_client_cb(esp_ble_mesh_cfg_client_cb_event_t event,
                                              esp_ble_mesh_cfg_client_cb_param_t *param)
{
    esp_ble_mesh_client_common_param_t common = {0};
    esp_ble_mesh_node_info_t *node = NULL;
    uint32_t opcode;
    uint16_t addr;
    int err;
    opcode = param->params->opcode;
    addr = param->params->ctx.addr;
    ESP_LOGI(TAG, "%s, error_code = 0x%02x, event = 0x%02x, addr: 0x%04x, opcode: 0x%04x",
             __func__, param->error_code, event, param->params->ctx.addr, opcode);

    if (param->error_code)
    {
        ESP_LOGE(TAG, "Send config client message failed, opcode 0x%04x", opcode);
        return;
    }
    node = ble_mesh_get_node_info(addr);
    if (!node)
    {
        ESP_LOGE(TAG, "%s: Get node info failed", __func__);
        return;
    }
    switch (event)
    {
    case ESP_BLE_MESH_CFG_CLIENT_GET_STATE_EVT:
        switch (opcode)
        {
        case ESP_BLE_MESH_MODEL_OP_COMPOSITION_DATA_GET:
        {
            ESP_LOGI(TAG, "composition data %s", bt_hex(param->status_cb.comp_data_status.composition_data->data, param->status_cb.comp_data_status.composition_data->len));
            esp_ble_mesh_cfg_client_set_state_t set_state = {0};
            ble_mesh_set_msg_common(&common, node, config_client.model, ESP_BLE_MESH_MODEL_OP_APP_KEY_ADD);
            set_state.app_key_add.net_idx = prov_key.net_idx;
            set_state.app_key_add.app_idx = prov_key.app_idx;
            memcpy(set_state.app_key_add.app_key, prov_key.app_key, 16);
            err = esp_ble_mesh_config_client_set_state(&common, &set_state);
            if (err)
            {
                ESP_LOGE(TAG, "%s: Config AppKey Add failed", __func__);
                return;
            }
            break;
        }
        default:
            break;
        }
        break;
    case ESP_BLE_MESH_CFG_CLIENT_SET_STATE_EVT:
        switch (opcode)
        {
        case ESP_BLE_MESH_MODEL_OP_APP_KEY_ADD:
        {
            /*bind appkey to on/off server in primary element*/
            esp_ble_mesh_cfg_client_set_state_t set_state = {0};
            ble_mesh_set_msg_common(&common, node, config_client.model, ESP_BLE_MESH_MODEL_OP_MODEL_APP_BIND);
            set_state.model_app_bind.element_addr = node->unicast;
            set_state.model_app_bind.model_app_idx = prov_key.app_idx;
            set_state.model_app_bind.model_id = ESP_BLE_MESH_MODEL_ID_GEN_ONOFF_SRV;
            set_state.model_app_bind.company_id = ESP_BLE_MESH_CID_NVAL;
            err = esp_ble_mesh_config_client_set_state(&common, &set_state);
            if (err)
            {
                ESP_LOGE(TAG, "%s: Config Model App Bind failed", __func__);
                return;
            }
            break;
        }
        case ESP_BLE_MESH_MODEL_OP_MODEL_APP_BIND:
        {
            break;
        }
        default:
            break;
        }
        break;
    case ESP_BLE_MESH_CFG_CLIENT_PUBLISH_EVT:
        switch (opcode)
        {
        case ESP_BLE_MESH_MODEL_OP_COMPOSITION_DATA_STATUS:
            ESP_LOG_BUFFER_HEX("composition data %s", param->status_cb.comp_data_status.composition_data->data,
                               param->status_cb.comp_data_status.composition_data->len);
            break;
        case ESP_BLE_MESH_MODEL_OP_APP_KEY_STATUS:
            break;
        default:
            break;
        }
        break;
    case ESP_BLE_MESH_CFG_CLIENT_TIMEOUT_EVT:
        switch (opcode)
        {
        case ESP_BLE_MESH_MODEL_OP_COMPOSITION_DATA_GET:
        {
            esp_ble_mesh_cfg_client_get_state_t get_state = {0};
            ble_mesh_set_msg_common(&common, node, config_client.model, ESP_BLE_MESH_MODEL_OP_COMPOSITION_DATA_GET);
            get_state.comp_data_get.page = COMP_DATA_PAGE_0;
            err = esp_ble_mesh_config_client_get_state(&common, &get_state);
            if (err)
            {
                ESP_LOGE(TAG, "%s: Config Composition Data Get failed", __func__);
                return;
            }
            break;
        }
        case ESP_BLE_MESH_MODEL_OP_APP_KEY_ADD:
        {
            esp_ble_mesh_cfg_client_set_state_t set_state = {0};
            ble_mesh_set_msg_common(&common, node, config_client.model, ESP_BLE_MESH_MODEL_OP_APP_KEY_ADD);
            set_state.app_key_add.net_idx = prov_key.net_idx;
            set_state.app_key_add.app_idx = prov_key.app_idx;
            memcpy(set_state.app_key_add.app_key, prov_key.app_key, 16);
            err = esp_ble_mesh_config_client_set_state(&common, &set_state);
            if (err)
            {
                ESP_LOGE(TAG, "%s: Config AppKey Add failed", __func__);
                return;
            }
            break;
        }
        case ESP_BLE_MESH_MODEL_OP_MODEL_APP_BIND:
        {
            esp_ble_mesh_cfg_client_set_state_t set_state = {0};
            ble_mesh_set_msg_common(&common, node, config_client.model, ESP_BLE_MESH_MODEL_OP_MODEL_APP_BIND);
            set_state.model_app_bind.element_addr = node->unicast;
            set_state.model_app_bind.model_app_idx = prov_key.app_idx;
            set_state.model_app_bind.model_id = ESP_BLE_MESH_MODEL_ID_SCENE_SRV;
            set_state.model_app_bind.company_id = ESP_BLE_MESH_CID_NVAL;
            err = esp_ble_mesh_config_client_set_state(&common, &set_state);
            if (err)
            {
                ESP_LOGE(TAG, "%s: Config Model App Bind failed", __func__);
                return;
            }

            break;
        }
        default:
            break;
        }
        break;
    default:
        ESP_LOGE(TAG, "Not a config client status message event");
        break;
    }
}
QueueHandle_t xQueue1;

static void updateStateDeviceTask (void *arg)
{   
    xQueue1= xQueueCreate(20, sizeof(dataUpdateState_t));
    dataUpdateState_t dataUpdateState;
    while (1)
    {
        if(xQueueReceive(xQueue1, &dataUpdateState, portMAX_DELAY))
        {   
            model_ack_state_t model_ack_state;
            model_ack_state.addr= dataUpdateState.addr;
            model_ack_state.stateinGW= dataUpdateState.state;
            Ble_mesh_Pub_state(dataUpdateState.addr, dataUpdateState.state);
            ble_mesh_custom_ack_state_message_set(&model_ack_state);
        }
        // vTaskDelay(6000/portTICK_RATE_MS);
        printf("stack free of task subcribeDeviceTask: %d \n", (int)uxTaskGetStackHighWaterMark(NULL));
    }
    
}
static void example_ble_mesh_generic_client_cb(esp_ble_mesh_generic_client_cb_event_t event,
                                               esp_ble_mesh_generic_client_cb_param_t *param)
{
    esp_ble_mesh_client_common_param_t common = {0};
    esp_ble_mesh_node_info_t *node = NULL;
    uint32_t opcode;
    uint16_t addr;
    int err;

    opcode = param->params->opcode;
    addr = param->params->ctx.addr;

    ESP_LOGI(TAG, "%s, error_code = 0x%02x, event = 0x%02x, addr: 0x%04x, opcode: 0x%04x",
             __func__, param->error_code, event, param->params->ctx.addr, opcode);

    if (param->error_code)
    {
        ESP_LOGE(TAG, "Send generic client message failed, opcode 0x%04x", opcode);
        return;
    }

    node = ble_mesh_get_node_info(addr);
    if (!node)
    {
        ESP_LOGE(TAG, "%s: Get node info failed", __func__);
        return;
    }

    switch (event)
    {
    case ESP_BLE_MESH_GENERIC_CLIENT_GET_STATE_EVT:
        switch (opcode)
        {
        case ESP_BLE_MESH_MODEL_OP_GEN_ONOFF_GET:
        {
            esp_ble_mesh_generic_client_set_state_t set_state = {0};
            node->onoff = param->status_cb.onoff_status.present_onoff;
            ESP_LOGI(TAG, "ESP_BLE_MESH_MODEL_OP_GEN_ONOFF_GET onoff: 0x%02x", node->onoff);
            /* After Generic OnOff Status for Generic OnOff Get is received, Generic OnOff Set will be sent */
            ble_mesh_set_msg_common(&common, node, onoff_client.model, ESP_BLE_MESH_MODEL_OP_GEN_ONOFF_SET);
            set_state.onoff_set.op_en = false;
            set_state.onoff_set.onoff = node->onoff;
            set_state.onoff_set.tid = 0;
            err = esp_ble_mesh_generic_client_set_state(&common, &set_state);
            if (err)
            {
                ESP_LOGE(TAG, "%s: Generic OnOff Set failed", __func__);
                return;
            }
            break;
        }
        case ESP_BLE_MESH_MODEL_OP_GEN_ONOFF_STATUS:
            ESP_LOGI(TAG, "ESP_BLE_MESH_MODEL_OP_GEN_ONOFF_STATUS");
            break;
        default:
            break;
        }
        break;
    case ESP_BLE_MESH_GENERIC_CLIENT_SET_STATE_EVT:
        switch (opcode)
        {
        case ESP_BLE_MESH_MODEL_OP_GEN_ONOFF_SET:
            node->onoff = param->status_cb.onoff_status.present_onoff;
            ESP_LOGI(TAG, "ESP_BLE_MESH_MODEL_OP_GEN_ONOFF_SET onoff: 0x%02x", node->onoff);
            break;
        case ESP_BLE_MESH_MODEL_OP_GEN_ONOFF_STATUS:
            ESP_LOGI(TAG, "ESP_BLE_MESH_MODEL_OP_GEN_ONOFF_STATUS");
            break;
        default:
            break;
        }
        break;
    case ESP_BLE_MESH_GENERIC_CLIENT_PUBLISH_EVT:
        ESP_LOGI(TAG, "ESP_BLE_MESH_GENERIC_CLIENT_PUBLISH_EVTYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYy: 0x%04x, 0x%02x", param->params->ctx.addr, param->status_cb.onoff_status.present_onoff);
        dataUpdateState_t dataUpdateState;
        dataUpdateState.addr = param->params->ctx.addr;
        dataUpdateState.state =  param->status_cb.onoff_status.present_onoff;
         xQueueSend(xQueue1, &dataUpdateState, 100);

        // Ble_mesh_Pub_state(param->params->ctx.addr, param->status_cb.onoff_status.present_onoff);
        switch (opcode)
        {
        case ESP_BLE_MESH_MODEL_OP_GEN_ONOFF_STATUS:
            ESP_LOGI(TAG, "addr 0x%04x , state: 0x%02x", param->params->ctx.addr, param->status_cb.onoff_status.present_onoff);
            break;

        default:
            break;
        }
        break;
    case ESP_BLE_MESH_GENERIC_CLIENT_TIMEOUT_EVT:
        /* If failed to receive the responses, these messages will be resend */
        switch (opcode)
        {
        case ESP_BLE_MESH_MODEL_OP_GEN_ONOFF_GET:
        {
            esp_ble_mesh_generic_client_get_state_t get_state = {0};
            ble_mesh_set_msg_common(&common, node, onoff_client.model, ESP_BLE_MESH_MODEL_OP_GEN_ONOFF_GET);
            err = esp_ble_mesh_generic_client_get_state(&common, &get_state);
            if (err)
            {
                ESP_LOGE(TAG, "%s: Generic OnOff Get failed", __func__);
                return;
            }
            break;
        }
        case ESP_BLE_MESH_MODEL_OP_GEN_ONOFF_SET:
        {
            // esp_ble_mesh_generic_client_set_state_t set_state = {0};
            // node->onoff = param->status_cb.onoff_status.present_onoff;
            // ESP_LOGI(TAG, "resend addd: 0x%04x: onoff: 0x%02x",node->unicast, node->onoff);
            // ble_mesh_set_msg_common(&common, node, onoff_client.model, ESP_BLE_MESH_MODEL_OP_GEN_ONOFF_SET);
            // set_state.onoff_set.op_en = false;
            // set_state.onoff_set.onoff = node->onoff;
            // set_state.onoff_set.tid = 0;
            // err = esp_ble_mesh_generic_client_set_state(&common, &set_state);
            // if (err)
            // {
            //     ESP_LOGE(TAG, "%s: Generic OnOff Set failed", __func__);
            //     return;
            // }
            resendwhenTimeout(addr, &common, node,  onoff_client.model);
            break;
        }
        default:
            break;
        }
        break;
    default:
        ESP_LOGE(TAG, "Not a generic client status message event");
        break;
    }
}

static void ble_mesh_client_scene_cb(esp_ble_mesh_time_scene_client_cb_event_t event, esp_ble_mesh_time_scene_client_cb_param_t *param)
{

    switch (event)
    {
    case ESP_BLE_MESH_TIME_SCENE_CLIENT_SET_STATE_EVT:
        ESP_LOGI(TAG, " ESP_BLE_MESH_TIME_SCENE_CLIENT_SET_STATE_EVT ");
        break;
    case ESP_BLE_MESH_TIME_SCENE_CLIENT_PUBLISH_EVT:
        ESP_LOGI(TAG, "ESP_BLE_MESH_TIME_SCENE_CLIENT_PUBLISH_EVT ");
        break;
    case ESP_BLE_MESH_TIME_SCENE_CLIENT_TIMEOUT_EVT:
        ESP_LOGI(TAG, "TIME OUT ROI ");
        break;

    default:
        break;
    }
}
#define BIT_WAIT (1<<0)
EventGroupHandle_t xCreatedEventGroup;
uint16_t DeviceArrd;
char DeviceID[30];
bool is_recvData=false;

 static void subcribeDeviceTask (void * arg)
 { 
    xCreatedEventGroup = xEventGroupCreate();
    while (1)
    {    
        xEventGroupWaitBits(xCreatedEventGroup, BIT_WAIT, pdTRUE, pdFALSE, portMAX_DELAY);
        if (Parse_device_infor(DeviceID))
            { 
                for( uint8_t i=0; i<ARRAY_SIZE(nodes); i++)
                {
                    if(nodes[i].unicast == DeviceArrd)
                    {
                        strcpy(nodes[i].Device_Id , Device_Id );
                        printf("Device_Id : %s\n", nodes[i].Device_Id);
                        FlashHandler_setData(NODE_STORE , "node_store",&nodes, sizeof(nodes));
                        break;
                    } 
                }
            }
         printf("stack free of task subcribeDeviceTask: %d \n", (int)uxTaskGetStackHighWaterMark(NULL));
        Uart_send_MqttData(Device_Id, strlen(Device_Id), UART_MQTT_SUB_DEVICE_TOPIC_OPCODE);
    }
 }
    
static void ble_mesh_custom_client_model_cb(esp_ble_mesh_model_cb_event_t event,
                                                   esp_ble_mesh_model_cb_param_t *param)
{
    switch (event)
    {
    case ESP_BLE_MESH_MODEL_OPERATION_EVT:
        switch (param->model_operation.opcode)
        {
        case ESP_BLE_MESH_CUSTOM_SENSOR_MODEL_OP_STATUS:
            ESP_LOGI(TAG, "OP_STATUS : 0x%06x", param->model_operation.opcode);
            ESP_LOG_BUFFER_HEX(TAG, param->model_operation.msg, param->model_operation.length);
            break;

        case ESP_BLE_MESH_CUSTOM_CONTROL_MODEL_OP_STATUS:
            ESP_LOGI(TAG, "OP_STATUS : 0x%06x", param->model_operation.opcode);
            ESP_LOG_BUFFER_HEX(TAG, param->model_operation.msg, param->model_operation.length);
            break;
        case ESP_BLE_MESH_CUSTOM_INFOR_FACTORY_OP_SET:
            ESP_LOGI(TAG, "ESP_BLE_MESH_CUSTOM_INFOR_FACTORY_OP_SET");
            // strcpy(DeviceID, (char *)param->model_operation.msg); 
            memcpy((uint8_t *)DeviceID, (uint8_t *)param->model_operation.msg, param->model_operation.length);
            DeviceID[ param->model_operation.length]=0;
            DeviceArrd = param->model_operation.ctx->addr;
            xEventGroupSetBitsFromISR(xCreatedEventGroup, BIT_WAIT, pdFALSE);
            break;
        default:
            ESP_LOGW(TAG, "Received unrecognized OPCODE message");
            break;
        }
        break;
    case ESP_BLE_MESH_MODEL_SEND_COMP_EVT:
        if (param->model_send_comp.err_code)
        {
            ESP_LOGE(TAG, "ESP_BLE_MESH_MODEL_SEND_COMP_EVT OPCODE 0x%06x", param->model_send_comp.opcode);
            break;
        }
        ESP_LOGI(TAG, "ESP_BLE_MESH_MODEL_SEND_COMP_EVT OPCODE 0x%06x ", param->model_send_comp.opcode);
        break;
    case ESP_BLE_MESH_CLIENT_MODEL_RECV_PUBLISH_MSG_EVT:
        switch (param->client_recv_publish_msg.opcode)
        {
        case ESP_BLE_MESH_CUSTOM_SENSOR_MODEL_OP_STATUS:
            ESP_LOGI(TAG, "OP_STATUS : 0x%06x", param->client_recv_publish_msg.opcode);
            ESP_LOG_BUFFER_HEX(TAG, param->client_recv_publish_msg.msg, param->client_recv_publish_msg.length);
            // model_scene_onoff_data_t received_data;
            // parse_received_data(param, &received_data);
            break;
        case ESP_BLE_MESH_CUSTOM_CONTROL_MODEL_OP_STATUS:
            ESP_LOGI(TAG, " DATA PUB FROM SRV");
            //    model_control_data_t received_ctrl_data;
            //     parse_rev_data_model_ctr(param, &received_ctrl_data);
            break;
        default:
            ESP_LOGW(TAG, "Received OPCODE message: 0x%04x", param->client_recv_publish_msg.opcode);
            break;
        }
        break;
    case ESP_BLE_MESH_CLIENT_MODEL_SEND_TIMEOUT_EVT:
        ESP_LOGW(TAG, "Mensagem 0x%06x timeout", param->client_send_timeout.opcode);
        break;
    default:
        ESP_LOGW(TAG, "%s - Unrecognized event: 0x%04x", __func__, event);
        break;
    }
}

/* init ble mesh */

esp_err_t ble_mesh_init(void)
{
    esp_err_t err = ESP_OK;
    err = bluetooth_init();
    if (err)
    {
        ESP_LOGE(TAG, "esp32_bluetooth_init failed (err %d)", err);
        return;
    }
    ble_mesh_get_dev_uuid(dev_uuid);
    uint8_t match[2] = {0xdd, 0xdd};
    prov_key.net_idx = ESP_BLE_MESH_KEY_PRIMARY;
    prov_key.app_idx = APP_KEY_IDX;
    memset(prov_key.app_key, APP_KEY_OCTET, sizeof(prov_key.app_key));
    esp_ble_mesh_register_prov_callback(example_ble_mesh_provisioning_cb);
    esp_ble_mesh_register_config_client_callback(example_ble_mesh_config_client_cb);
    esp_ble_mesh_register_generic_client_callback(example_ble_mesh_generic_client_cb);
    esp_ble_mesh_register_time_scene_client_callback(ble_mesh_client_scene_cb);
    esp_ble_mesh_register_custom_model_callback(ble_mesh_custom_client_model_cb);
    err = esp_ble_mesh_init(&provision, &composition);
    if (err != ESP_OK)
    {
        ESP_LOGE(TAG, "Failed to initialize mesh stack (err %d)", err);
        return err;
    }
    err = esp_ble_mesh_client_model_init(&custom_models[0]);
    if (err)
    {
        ESP_LOGE(TAG, "Failed to initialize vendor client");
        return err;
    }
    err = esp_ble_mesh_client_model_init(&custom_models[1]);
    if (err)
    {
        ESP_LOGE(TAG, "Failed to initialize vendor client");
        return err;
    }
    err = esp_ble_mesh_client_model_init(&custom_models[2]);
    if (err)
    {
        ESP_LOGE(TAG, "Failed to initialize vendor client");
        return err;
    }
    err = esp_ble_mesh_provisioner_set_dev_uuid_match(match, sizeof(match), 0x0, false);
    if (err != ESP_OK)
    {
        ESP_LOGE(TAG, "Failed to set matching device uuid (err %d)", err);
        return err;
    }
    err = esp_ble_mesh_provisioner_prov_enable(ESP_BLE_MESH_PROV_ADV | ESP_BLE_MESH_PROV_GATT);
    if (err != ESP_OK)
    {
        ESP_LOGE(TAG, "Failed to enable mesh provisioner (err %d)", err);
        return err;
    }
    err = esp_ble_mesh_provisioner_add_local_app_key(prov_key.app_key, prov_key.net_idx, prov_key.app_idx);
    if (err != ESP_OK)
    {
        ESP_LOGE(TAG, "Failed to add local AppKey (err %d)", err);
        return err;
    }
    ESP_LOGI(TAG, "BLE Mesh Provisioner initialized");
    return err;
    // xCreatedEventGroup = xEventGroupCreate();
}
void taskinit()
{
    xTaskCreate(subcribeDeviceTask, "subcribeDeviceTask", 1024*3, NULL, 10, NULL);
    xTaskCreate(updateStateDeviceTask, "updateStateDeviceTask", 1024*5, NULL, 10, NULL);
}