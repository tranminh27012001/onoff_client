#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "freertos/event_groups.h"
#include "driver/uart.h"
#include "esp_log.h"

#include "Uart_handle.h"
#include "MKP_Uart_MQTT.h"
#include "Datetime.h"

/*******************************************************************************
 * Definitions
 ******************************************************************************/
#define UART_TO_SERVER UART_NUM_1
#define BUF_SIZE (1024)
#define RD_BUF_SIZE (BUF_SIZE)
static QueueHandle_t uart0_queue;
extern QueueHandle_t g_UartMesQueueHandle;

/*******************************************************************************
 * Variables
******************************************************************************/
static const char *TAG = "uart_events";
extern EventGroupHandle_t messHttpRecved_Event;
static QueueHandle_t messUartHandle_Queue;
uint16_t s_UartmessLengOffset =0 ;
uint8_t Buf_Recv[MAX_LENG_RX_BUF];
char *MessHttpRecv = NULL;
uint16_t s_LengdataHttpOffset =0;
bool s_IsFirstmessHttp = true ;
bool s_Is_recv_full;
extern uint64_t g_timeSync;
extern bool g_mustSynTime;
uint16_t lengMessPayload =0;
size_t s_lengUartRecv;

/*******************************************************************************
 * Prototypes
 ******************************************************************************/
static uint8_t Cks(uint8_t * Buff_Frame, int leng_data);
static bool checkByteStop (uint8_t *dtmp, uint16_t lengOffsett, uint16_t lengPayload);
/*******************************************************************************
 * Code
 ******************************************************************************/

static bool checkByteStop (uint8_t *dtmp, uint16_t lengOffsett, uint16_t lengPayload)
{
    uint16_t idx_byteStop = lengPayload + 7 - lengOffsett;
    if(dtmp[idx_byteStop] == BYTE_STOP)
    {
        return true;
    }
    return false;
}
static void uartMess_handle_task(void *arg)
{       
    uint8_t dtmp[120]={0};
    while (1)
    {
        if(xQueueReceive(messUartHandle_Queue, dtmp, portMAX_DELAY))
        {
            uint8_t *ByteStart =NULL;
            size_t size = s_lengUartRecv ;
            ByteStart = dtmp ;
            if(ByteStart[0] == BYTE_START)
            {   
                bzero(Buf_Recv, MAX_LENG_RX_BUF);
                s_UartmessLengOffset =0;
                lengMessPayload = ByteStart[BYTE_LSBLENGPAYLOAD] | (ByteStart[BYTE_MSBLENGPAYLOAD] << 8);  
                ESP_LOGI(TAG,"lengMessPayload : %d \n", lengMessPayload);
                if(checkByteStop(dtmp, s_UartmessLengOffset, lengMessPayload))
                {
                    memcpy(Buf_Recv, dtmp, lengMessPayload +8);
                    s_Is_recv_full =true;
                }
                else
                {
                    memcpy(Buf_Recv,dtmp, size);
                    s_UartmessLengOffset += size;
                    s_Is_recv_full =false;
                }
            }
            else
            {   
                if(checkByteStop(dtmp, s_UartmessLengOffset, lengMessPayload))
                {   
                    memcpy(Buf_Recv+ s_UartmessLengOffset, dtmp, lengMessPayload + 8 - s_UartmessLengOffset );
                    s_Is_recv_full =true;
                }
                else
                {
                    memcpy(Buf_Recv + s_UartmessLengOffset, dtmp, size);
                    s_UartmessLengOffset += size ;
                    s_Is_recv_full=false;
                }
            }   
            if(s_Is_recv_full)
            {   
                printf("da nhan fulll \n");
                if(Buf_Recv[BYTE_EVENT]==UART_MQTT_EVENT)
                {   
                    mqttMes_t newmess;
                    uint8_t ARR[230];
                    memcpy(ARR, Buf_Recv + IDX_ADRR_PAYLOAD, sizeof(mqttMes_t));
                    xQueueSend(g_UartMesQueueHandle, ARR, 1000 / portTICK_RATE_MS);
                }
                else if(Buf_Recv[BYTE_EVENT]==UART_HTTP_EVENT)
                {
                    if(Buf_Recv[BYTE_OPCODE]==UART_HTTP_SENDING_OPCODE && s_IsFirstmessHttp)
                    {
                        MessHttpRecv = (char *)malloc(lengMessPayload);
                        memcpy(MessHttpRecv,(char *)(Buf_Recv+IDX_ADRR_PAYLOAD), lengMessPayload);
                        s_LengdataHttpOffset +=lengMessPayload;
                        s_IsFirstmessHttp =false;
                    }
                    else if(Buf_Recv[BYTE_OPCODE]==UART_HTTP_SENDING_OPCODE && !s_IsFirstmessHttp)
                    {
                        MessHttpRecv = (char *)realloc(MessHttpRecv, lengMessPayload + s_LengdataHttpOffset);
                        memcpy(MessHttpRecv+s_LengdataHttpOffset,(char *)(Buf_Recv+IDX_ADRR_PAYLOAD), lengMessPayload);
                        s_LengdataHttpOffset +=lengMessPayload; 
                    }
                    else if(Buf_Recv[BYTE_OPCODE]==UART_HTTP_FINISH_OPCODE)
                    {   
                        MessHttpRecv = (char *)realloc(MessHttpRecv, lengMessPayload + s_LengdataHttpOffset);
                        memcpy(MessHttpRecv+s_LengdataHttpOffset,(char *)(Buf_Recv+IDX_ADRR_PAYLOAD), lengMessPayload);
                        lengMessPayload += s_LengdataHttpOffset ;
                        MessHttpRecv[lengMessPayload] = 0;
                        xEventGroupSetBits(messHttpRecved_Event, BIT_RECV_FULL_MESS);
                        s_IsFirstmessHttp =true;
                        s_LengdataHttpOffset =0;
                    }
                }
                else if(Buf_Recv[BYTE_EVENT]==UART_TIME_SYN_EVENT)
                {   
                    uint64_t *valueTime = NULL;
                    valueTime = (uint64_t *)(Buf_Recv+IDX_ADRR_PAYLOAD);
                    g_timeSync = *valueTime ;
                    g_mustSynTime=true;
                    printf("time sync: \n");
                }
                bzero(dtmp, 120);
            }
        }
    }
}
uint8_t messs[120];
static void uart_event_task(void *pvParameters)
{
    uart_event_t event;
    size_t buffered_size;
    uint8_t *dtmp = (uint8_t *)malloc(RD_BUF_SIZE);
    for (;;)
    {
        // Waiting for UART event.
        if (xQueueReceive(uart0_queue, (void *)&event, (portTickType)portMAX_DELAY))
        {
            bzero(dtmp, RD_BUF_SIZE);
            switch (event.type)
            {
            case UART_DATA:
                uart_read_bytes(UART_TO_SERVER, dtmp, event.size, portMAX_DELAY);
                s_lengUartRecv = event.size;
                memcpy(messs, dtmp, event.size);
                xQueueSend(messUartHandle_Queue, messs, 100);                       
                break;
            case UART_FIFO_OVF:
                ESP_LOGI(TAG, "hw fifo overflow");

                uart_flush_input(UART_TO_SERVER);
                xQueueReset(uart0_queue);
                break;

            case UART_BUFFER_FULL:
                ESP_LOGI(TAG, "ring buffer full");

                uart_flush_input(UART_TO_SERVER);
                xQueueReset(uart0_queue);
                break;

            case UART_BREAK:
                ESP_LOGI(TAG, "uart rx break");
                break;

            case UART_PARITY_ERR:
                ESP_LOGI(TAG, "uart parity error");
                break;

            case UART_FRAME_ERR:
                ESP_LOGI(TAG, "uart frame error");
                break;

            case UART_PATTERN_DET:
                break;
            // Others
            default:
                ESP_LOGI(TAG, "uart event type: %d", event.type);
                break;
            }
        }
    }
    free(dtmp);
    dtmp = NULL;
    vTaskDelete(NULL);
}

void Uart_Gw_init(void)
{
    esp_log_level_set(TAG, ESP_LOG_INFO);
    uart_config_t uart_config = {
        .baud_rate = 115200,
        .data_bits = UART_DATA_8_BITS,
        .parity = UART_PARITY_DISABLE,
        .stop_bits = UART_STOP_BITS_1,
        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
        .source_clk = UART_SCLK_APB,
    };

    uart_driver_install(UART_TO_SERVER, BUF_SIZE * 2, BUF_SIZE * 2, 20, &uart0_queue, 0);
    uart_param_config(UART_TO_SERVER, &uart_config);

    esp_log_level_set(TAG, ESP_LOG_INFO);

    uart_set_pin(UART_TO_SERVER, UART_TX, UART_RX, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE);
    // uart_set_pin(UART_TO_SERVER, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE);
    messUartHandle_Queue = xQueueCreate(20, 130);
    xTaskCreate(uart_event_task, "uart_event_task", 1024*3, NULL, 12, NULL);
    xTaskCreate(uartMess_handle_task, "uartMess_handle_task", 1024 *5, NULL, 10, NULL);
}
static uint8_t Cks(uint8_t * Buff_Frame , int leng_data)
{   
    uint8_t temp = Buff_Frame[3];
    for(uint8_t i=4; i<leng_data+3; i++)
    {
        temp ^=Buff_Frame[i];
    }
    return temp;
}

static void Uart_send_Frame(void * data_send, uint16_t leng_data, uint8_t uart_type, uint8_t opcode)
{
    uint8_t Frame_Data[MAX_LENG_TX_BUF];
    uint16_t leng_Frame =0;
    Frame_Data[leng_Frame++] = BYTE_START ;  // BYTE START
    Frame_Data[leng_Frame++] = uart_type ;  // EVENT : MQTT, HTTP
    Frame_Data[leng_Frame++] = opcode ;  // opcode of event uart
    Frame_Data[leng_Frame++] = CF_START(opcode, uart_type);
    Frame_Data[leng_Frame++] = leng_data & 0xFF;
    Frame_Data[leng_Frame++] = (leng_data>>8)& 0xFF;
    memcpy(Frame_Data+leng_Frame,data_send, leng_data );
    leng_Frame +=leng_data;
    Frame_Data[leng_Frame++] = Cks(Frame_Data,leng_data);
    Frame_Data[leng_Frame++] = BYTE_STOP;
    // printf("size:%d, cks :0x%02x",leng_Frame, Frame_Data[ leng_Frame-2]);
    if(uart_write_bytes(UART_TO_SERVER ,Frame_Data, leng_Frame)) ESP_LOGI(TAG, " SEND UART OKE");
}

void Uart_send_MqttData (void * data_send, int leng_data, uint8_t opcode)
{
    Uart_send_Frame(data_send, leng_data, UART_MQTT_EVENT, opcode);
}
void Uart_send_HttpData(void * data_send, int leng_data, uint8_t opcode)
{
    Uart_send_Frame(data_send, leng_data, UART_HTTP_EVENT, opcode);
}
void Uart_send_GetblobScheParam(void* getSchedule_param, int leng_data)
{
    Uart_send_Frame(getSchedule_param, sizeof(getSchedule_param_t), UART_HTTP_EVENT, UART_HTTP_GET_BLOB_SCHE_OPCODE);
}


// Function Handle send

