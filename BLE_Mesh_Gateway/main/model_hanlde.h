#ifndef _MODEL_HANDLE_
#define _MODEL_HANDLE_

#include "esp_err.h"
#include "custom_model_defs.h"

// typedef struct 
// {
//    uint16_t addr;
//    uint8_t state;
   
// }Data_Uart_onoff_t;

// typedef struct 
// {
//   uint16_t scene_delete ;
// }model_delete_scene__t;

// typedef struct 
// {
//    uint16_t addr ;
//    char  Device_Id[20] ;
//    char  Device_Type[20];
// }model_Send_note_infor_t;


/* HANDLE OF GENERIC ONOFF CLIENT*/
void Send_onoff_to_Device (char *g_product_id, uint8_t local_id,uint8_t state,  uint8_t mid );
void Ble_mesh_Pub_state (uint16_t addr, uint8_t state) ;
void example_ble_mesh_send_gen_onoff_get(uint16_t addr);

/*HANDLE OF SCENE MODEL*/
void handle_client_scene_set_store( uint16_t scene_number_) ;
void handle_client_scene_delete( uint16_t scene_number_ ) ;
void handle_client_scene_set_ReCall ( uint16_t scene_number_, uint16_t net_idx, uint16_t app_idx) ;

/*custom handle*/
esp_err_t ble_mesh_custom_elm_control_client_model_message_set(model_control_data_t *set_data, uint16_t addr);
esp_err_t ble_mesh_custom_scene_onoff_message_set(model_scene_onoff_data_t *set_data) ;
esp_err_t ble_mesh_custom_ack_state_message_set(model_ack_state_t *ack_param);

/*parse device data*/
bool Parse_device_infor (char * data_recv);
// void set_scene_and_elm_control(void) ;
#endif