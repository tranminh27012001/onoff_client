#ifndef _UART_GW_
#define _UART_GW_

#include <stdint.h>

#define UART_RX 37
#define UART_TX 36
#define MES_UART_RECV_FULL (1<<0)
// #define BYTE_START 0x1F
#define BYTE_START 0x7E
#define BYTE_STOP 0x5E
#define CF_START(x,y) (x^y)
#define CF_STOP(x,y) (x^y)

#define LEN_FRAME_UART 250
#define LEN_BUF_SEND 130
#define MAX_LENG_RX_BUF 3000 
#define MAX_LENG_TX_BUF 300

enum s_topic_filter
{
    PRODUCT_ID,
    USER_NAME ,
    EVENT_TYPE,
    LOCAL_ID,
    PROPERTY_CODE
};

enum s_define_byteUart
{   
    BYTE_EVENT = 1,
    BYTE_OPCODE,
    BYTE_CF_START,
    // BYTE_GETTING_ALL_SCHE,
    // BYTE_GETTING_BLOB_SCHE,
    BYTE_LSBLENGPAYLOAD,
    BYTE_MSBLENGPAYLOAD,
    IDX_ADRR_PAYLOAD,
};
typedef enum 
{
    UART_MQTT_SEND_TOPIC_DATA = 0,       // SERVER TO GATEWAY
    UART_MQTT_UPDATE_STATE_NODE_OPCODE ,      // GATEWAY TO SERVER
    UART_MQTT_SUB_DEVICE_TOPIC_OPCODE,                 // GATEWAY TO SERVER
    UART_MQTT_EVENT,
    UART_HTTP_SENDING_OPCODE,
    UART_HTTP_FINISH_OPCODE,
    UART_HTTP_GET_BLOB_SCHE_OPCODE,
    UART_HTTP_EVENT,
    UART_TIME_SYN_EVENT,
}uart_type_t;
enum RX_STATE
{
    WAIT_START = 0,
    IN_MESG,
    STOP,
};
 
void Uart_Gw_init (void) ;
// void Uart_send (void * data_send , int leng_data);
// void Uart_send_Frame(void * data_send , int leng_data, uart_type_t uart_type);
void Uart_send_MqttData (void * data_send, int leng_data, uint8_t opcode);
void Uart_send_HttpData(void * data_send, int leng_data, uint8_t opcode);
void Uart_send_GetblobScheParam(void* getSchedule_param, int leng_data);
#endif