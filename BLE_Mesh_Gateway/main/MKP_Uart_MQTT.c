#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include "esp_system.h"
#include "nvs_flash.h"
#include "esp_event.h"
#include "esp_netif.h"
#include "cJSON.h"
#include "sdkconfig.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"

#include "esp_log.h"

#include "lwip/sockets.h"
#include "lwip/dns.h"
#include "lwip/netdb.h"


#include "mqtt_client.h"

#include "MKP_Uart_MQTT.h"
#include "Uart_handle.h"
#include "provisioner_gw.h"
#include "model_hanlde.h"

/*******************************************************************************
 * Definitions
 ******************************************************************************/
#ifndef DISABLE_LOG_I
#define MKP_UART_LOG_INFO_ON
#endif
#ifdef MKP_UART_LOG_INFO_ON
#define log_info(format, ...) ESP_LOGI("MKP_UART", format, ##__VA_ARGS__)
#else
#define log_info(format, ...)
#endif

#define PROTO_SWITCH "W_SWITCH"
#define PROTO_GET_INFO "W_GET_INFO"
#define PROTO_GET_TOUCH_INFO "W_GET_TOUCH"
#define PROTO_NUM_OF_IN_MSG 3
#define TAG "MKP_Uart_MQTT"

 /*******************************************************************************
 * Variables
 ******************************************************************************/
QueueHandle_t g_UartMesQueueHandle;
extern esp_ble_mesh_node_info_t nodes[CONFIG_BLE_MESH_MAX_PROV_NODES] ;
extern uint8_t Buf_Recv[3000];
char topic_filter[5][30] = {0};
typedef uint8_t (*parse_msg_t)(cJSON *data);
typedef struct
{
    const char *cmdType;
    parse_msg_t msgHandle;
} mqtt_in_msg_t;


/*******************************************************************************
 * Prototypes
 ******************************************************************************/
static uint8_t PROTO_ControlSW(cJSON *data) ;
const mqtt_in_msg_t mqttInMsgHandleTable[PROTO_NUM_OF_IN_MSG] =
    {
        {PROTO_SWITCH, PROTO_ControlSW},
    };

/*******************************************************************************
 * Code
 ******************************************************************************/
/*Handle send*/
void pubTopicFromProductId(char* g_product_Id, char *eventType, uint8_t localId,const char *propertyCode, char* topicName){
	sprintf(topicName,"d/%s/p/%s/%d/%s",g_product_Id,eventType,localId,propertyCode);
}   
void UART_TO_MQTT_PublishNewState(char *g_product_Id ,const char *proCode, uint8_t localId, int16_t state, char *mid, char *reason, cJSON *info)  // Func publish new state (reason: MQTT, touch,)
{
	// char pData[MAX_LEN_MSG] = {0};
	// char Topic_name[80] = {0};
	mqttMes_t pData ;
	cJSON *pData_info = NULL;
		if (mid != NULL) // ton tai mid
		{
			//if(cJSON_HasObjectItem(info, "i"))
			if (info != NULL)
			{
				sprintf(pData.data, "{\"d\":%d, \"rid\":\"%s\", \"r\":\"%s\"}", state, mid, reason);
				pData_info = cJSON_Parse(pData.data);                                    //  data , mid, nguyên nhân, 
				cJSON_AddItemToArray(pData_info, info);
				//pData = cJSON_Print(pData_info);
				sprintf(pData.data, cJSON_Print(pData_info));
			}
			else
			{
				sprintf(pData.data, "{\"d\":%d, \"rid\":\"%s\", \"r\":\"%s\"}", state, mid, reason);
			}
		}
		else // mid not exist
		{
			if (info != NULL)
			{
				sprintf(pData.data, "{\"d\":%d, \"r\":\"%s\"}", state, reason);
				pData_info = cJSON_Parse(pData.data);
				cJSON_AddItemToArray(pData_info, info);
				sprintf(pData.data, cJSON_Print(pData_info));
			}
			else
			{
				sprintf(pData.data, "{\"d\":%d, \"r\":\"%s\"}", state, reason);
			}
		}
	pubTopicFromProductId(g_product_Id,"UP", localId, proCode, pData.topic);
	cJSON_Delete(pData_info);
	printf("\n size pData:%d",sizeof(pData));
	ESP_LOGI(TAG,"TOPIC: %s", pData.topic);
	ESP_LOGI(TAG, "DATA: %s", pData.data);
	// Uart_send_Frame(&pData,sizeof(pData), UART_MQTT_UPDATE_STATE_NODE_OPCODE);
	Uart_send_MqttData(&pData, sizeof(pData), UART_MQTT_UPDATE_STATE_NODE_OPCODE);
}

/*Handle Recv*/
static void sub_Product_id (char *Prod_id , char * data_in)
{
    char * ptr;
    ptr = strtok (data_in,",");
    strcpy(Prod_id , ptr);
    printf("\n %s \n", Prod_id );
}
static void sub_string(char *des, char *src, int start, int end)
{
	int index = 0;
	for (int i = start + 1; i < end; i++)
	{
		des[index++] = src[i];
	}
	des[index] = '\0';
}
static void topic_name_filter(char *data, int len)
{
	// format topic: d/proId/s/username/event_type/localID/propertycode
	//  d/a4cf12987e60/s/tranminh27012001@gmail.com/CP/3/W_SWITCH
	// format ota topic: d/proId/s/admin/UF
	//count: chi so ki tu '/'
	int index[7], count = 0, count_sub = 0;
	for (int i = 0; i < len; i++)
	{
		if (data[i] == '/')
		{
			index[count++] = i;  // lấy vị trí của "/"
		}
	}
	index[count] = len; //cuoi cua topic name khong co dau "/" nen phai them chi so vao cuoi

	sub_string(topic_filter[count_sub++], data, index[0], index[1]); //productId
	for (int i = 2; i < count; i++)
	{
		sub_string(topic_filter[count_sub++], data, index[i], index[i + 1]);
	}
}

static uint8_t PROTO_ControlSW(cJSON *data)
{
    cJSON *info = NULL;
    uint8_t mid =0;
	uint8_t state =0 ; 
    if (cJSON_HasObjectItem(data, "d"))
    {
       state = cJSON_GetObjectItem(data, "d")->valueint ;
	   ESP_LOGI(TAG,"STATE : %d", state );
    }
    uint8_t local_id = (uint8_t)LOCAL_ID_SERVER_TO_DEVICE(atoi(topic_filter[LOCAL_ID]));
    printf("LOCAL_ID_SERVER_TO_DEVICE : %d",local_id );
	Send_onoff_to_Device(topic_filter[PRODUCT_ID], local_id, state ,10);
    return 0;
}

void PROTO_processCmd(char *data)  // data of MQtt 
{

    cJSON *msgObject;
	msgObject = cJSON_Parse(data);
    if (strcmp(topic_filter[EVENT_TYPE], EVT_CONTROL_PROPERTY) == 0) //event type: control property
    {
        if (strcmp(topic_filter[PROPERTY_CODE], PROTO_SWITCH) == 0)
        {
            PROTO_ControlSW(msgObject);
        }
    }
	free(msgObject);
}
static void PROTO_task()
{
	uint8_t Buff_Recv[300];
    g_UartMesQueueHandle = xQueueCreate(MES_QUEUE_LEN, sizeof(mqttMes_t));
    while (1)
    {
        if (xQueueReceive(g_UartMesQueueHandle, Buff_Recv, portMAX_DELAY))
        {	
			mqttMes_t *newData = (mqttMes_t *)Buff_Recv;
            topic_name_filter(newData->topic, strlen(newData->topic));
			ESP_LOGI(TAG, "%s", newData->topic);
			ESP_LOGI(TAG, "%s", newData->data);
            PROTO_processCmd(newData->data); // phần truyền dữ liệu xuống
        }
    }
}
void PROTO_init()
{
    xTaskCreate(PROTO_task, "PROTO_task", 5 * 1024, NULL, 4, NULL);
}


