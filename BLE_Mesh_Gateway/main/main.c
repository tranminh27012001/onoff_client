/*
-time : 1/2023
-author: Tran Minh
-company : Makipos
*/
#include <stdio.h>
#include <string.h>

#include "esp_log.h"
#include "nvs_flash.h"
  
#include "esp_ble_mesh_defs.h"
#include "esp_ble_mesh_common_api.h"
#include "esp_ble_mesh_provisioning_api.h"
#include "esp_ble_mesh_networking_api.h"
#include "esp_ble_mesh_config_model_api.h"
#include "esp_ble_mesh_generic_model_api.h"
#include "esp_ble_mesh_local_data_operation_api.h"
#include "esp_bt.h"

#include "ble_mesh_example_init.h"

#include "custom_model_defs.c"
#include "model_handle.c"
#include "prov_handle.h"
#include "provisioner_gw.h"

#include "Uart_handle.h"
#include "MKP_Uart_MQTT.h"
#include "FlashHandler.h"
#include "DateTime.h"

extern  esp_ble_mesh_node_info_t nodes[CONFIG_BLE_MESH_MAX_PROV_NODES] ;
extern uint64_t g_timeSync ;
extern bool g_mustSynTime;

void tx_power_set()
{
    esp_ble_tx_power_set(ESP_BLE_PWR_TYPE_CONN_HDL0, ESP_PWR_LVL_P18);
    esp_ble_tx_power_set(ESP_BLE_PWR_TYPE_CONN_HDL1, ESP_PWR_LVL_P18);
    esp_ble_tx_power_set(ESP_BLE_PWR_TYPE_CONN_HDL2, ESP_PWR_LVL_P18);
    esp_ble_tx_power_set(ESP_BLE_PWR_TYPE_CONN_HDL3, ESP_PWR_LVL_P18);
    esp_ble_tx_power_set(ESP_BLE_PWR_TYPE_CONN_HDL4, ESP_PWR_LVL_P18);
    esp_ble_tx_power_set(ESP_BLE_PWR_TYPE_CONN_HDL5, ESP_PWR_LVL_P18);
    esp_ble_tx_power_set(ESP_BLE_PWR_TYPE_CONN_HDL6, ESP_PWR_LVL_P18);
    esp_ble_tx_power_set(ESP_BLE_PWR_TYPE_CONN_HDL7, ESP_PWR_LVL_P18);
    esp_ble_tx_power_set(ESP_BLE_PWR_TYPE_CONN_HDL8, ESP_PWR_LVL_P18);
    esp_ble_tx_power_set(ESP_BLE_PWR_TYPE_ADV, ESP_PWR_LVL_P18);
    esp_ble_tx_power_set(ESP_BLE_PWR_TYPE_SCAN, ESP_PWR_LVL_P18);

}

void app_main(void)
{
    esp_err_t err;
    ESP_LOGI(TAG, "Initializing...");
    err = nvs_flash_init();
    if (err == ESP_ERR_NVS_NO_FREE_PAGES) {
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
    }
    ESP_ERROR_CHECK(err);
    // tx_power_set();
    err = ble_mesh_init();
    if (err) {
        ESP_LOGE(TAG, "Bluetooth mesh init failed (err %d)", err);
    }
    // taskinit();
    Uart_Gw_init();
    PROTO_init();
    FlashHandler_getData(NODE_STORE,"node_store", &nodes);
    initDateTime();
    taskinit();
    //    while (1)
    // {   
    //     // for(uint8_t i=0; i< ARRAY_SIZE(nodes); i++)
    //     // {
    //     //     printf("\nnodes[%d]: 0x%04x, %s", i, nodes[i].unicast, nodes[i].Device_Id);
    //     // }
    //     // printf("heap: %d\n", esp_get_free_internal_heap_size());
    //     printf("ESP_BLE_PWR_TYPE_CONN_HDL0: %d\n",esp_ble_tx_power_get(ESP_BLE_PWR_TYPE_CONN_HDL0));
    //     printf("ESP_BLE_PWR_TYPE_CONN_HDL1: %d\n",esp_ble_tx_power_get(ESP_BLE_PWR_TYPE_CONN_HDL1));
    //     printf("ESP_BLE_PWR_TYPE_CONN_HDL2: %d\n",esp_ble_tx_power_get(ESP_BLE_PWR_TYPE_CONN_HDL2));
    //     printf("ESP_BLE_PWR_TYPE_CONN_HDL3: %d\n",esp_ble_tx_power_get(ESP_BLE_PWR_TYPE_CONN_HDL3));
    //     printf("ESP_BLE_PWR_TYPE_CONN_HDL4: %d\n",esp_ble_tx_power_get(ESP_BLE_PWR_TYPE_CONN_HDL4));
    //     printf("ESP_BLE_PWR_TYPE_CONN_HDL5: %d\n",esp_ble_tx_power_get(ESP_BLE_PWR_TYPE_CONN_HDL5));
    //     printf("ESP_BLE_PWR_TYPE_CONN_HDL6: %d\n",esp_ble_tx_power_get(ESP_BLE_PWR_TYPE_CONN_HDL6));
    //     printf("ESP_BLE_PWR_TYPE_CONN_HDL7: %d\n",esp_ble_tx_power_get(ESP_BLE_PWR_TYPE_CONN_HDL7));
    //     printf("ESP_BLE_PWR_TYPE_CONN_HDL8: %d\n",esp_ble_tx_power_get(ESP_BLE_PWR_TYPE_CONN_HDL8));
    //     vTaskDelay(5000/ portTICK_RATE_MS);
    // }
    model_ack_state_t model_ack_state;
    model_ack_state.addr= 0x0005;
    model_ack_state.stateinGW= 0x01;
    // while (1)
    // {
    //     ble_mesh_custom_ack_state_message_set(&model_ack_state);
    //     vTaskDelay(3000/portTICK_RATE_MS);
    // }
    
}
