
#ifndef __CUSTOM_SENSOR_MODEL_DEFS_H__
#define __CUSTOM_SENSOR_MODEL_DEFS_H__
    
#include <stdio.h>
#include "sdkconfig.h"                                                                                                    
#include "esp_ble_mesh_common_api.h"

// Config Name
#define CONFIG_MESH_DEVICE_NAME "GATE WAY"
#define BLE_MESH_DEVICE_NAME CONFIG_MESH_DEVICE_NAME
#define CID_ESP 0x02E5
  
// define Model_id set scene custom
#define ESP_BLE_MESH_CUSTOM_SENSOR_MODEL_ID_SERVER 0x1414
#define ESP_BLE_MESH_CUSTOM_SENSOR_MODEL_ID_CLIENT 0x1415
// define Model control custom
#define ESP_BLE_MESH_CUSTOM_CONTROL_MODEL_ID_SERVER 0x1416
#define ESP_BLE_MESH_CUSTOM_CONTROL_MODEL_ID_CLIENT 0x1417
// define Model Ack pub state
#define ESP_BLE_MESH_CUSTOM_ACK_MODEL_ID_SERVER 0x1420
#define ESP_BLE_MESH_CUSTOM_ACK_MODEL_ID_CLIENT 0x1421 

/* define Model recv infor factory */
#define ESP_BLE_MESH_CUSTOM_INFOR_FACTORY_ID_SERVER 0x1418
/*define opcode*/
// opcode set scene 
#define ESP_BLE_MESH_CUSTOM_SENSOR_MODEL_OP_GET     ESP_BLE_MESH_MODEL_OP_3(0x00, CID_ESP)  
#define ESP_BLE_MESH_CUSTOM_SENSOR_MODEL_OP_SET     ESP_BLE_MESH_MODEL_OP_3(0x01, CID_ESP)
#define ESP_BLE_MESH_CUSTOM_SENSOR_MODEL_OP_STATUS   ESP_BLE_MESH_MODEL_OP_3(0x02, CID_ESP)
//opcode set control scene
#define ESP_BLE_MESH_CUSTOM_CONTROL_MODEL_OP_GET    ESP_BLE_MESH_MODEL_OP_3(0x03, CID_ESP)
#define ESP_BLE_MESH_CUSTOM_CONTROL_MODEL_OP_SET     ESP_BLE_MESH_MODEL_OP_3(0x04, CID_ESP)
#define ESP_BLE_MESH_CUSTOM_CONTROL_MODEL_OP_STATUS  ESP_BLE_MESH_MODEL_OP_3(0x05, CID_ESP)
//opcode recv infor factory
#define ESP_BLE_MESH_CUSTOM_INFOR_FACTORY_OP_GET    ESP_BLE_MESH_MODEL_OP_3(0x06, CID_ESP)                     
#define ESP_BLE_MESH_CUSTOM_INFOR_FACTORY_OP_SET     ESP_BLE_MESH_MODEL_OP_3(0x07, CID_ESP)
#define ESP_BLE_MESH_CUSTOM_INFOR_FACTORY_OP_STATUS  ESP_BLE_MESH_MODEL_OP_3(0x08, CID_ESP)
// opcode 
#define ESP_BLE_MESH_CUSTOM_ACK_STATE_OP_GET    ESP_BLE_MESH_MODEL_OP_3(0x09, CID_ESP)                     
#define ESP_BLE_MESH_CUSTOM_ACK_STATE_OP_SET     ESP_BLE_MESH_MODEL_OP_3(0x0A, CID_ESP)
#define ESP_BLE_MESH_CUSTOM_ACK_STATE_OP_STATUS  ESP_BLE_MESH_MODEL_OP_3(0x0B, CID_ESP)

#define ESP_BLE_MESH_GROUP_PUB_ADDR 0xC100
    
typedef struct 
{
    uint16_t scene ;
    uint16_t addrr ;
    uint8_t state ;
} model_scene_onoff_data_t;

typedef struct 
{
   uint16_t element_control_adrr;
   uint16_t scene ;
}model_control_data_t;

typedef struct 
{
   uint8_t data_infor[20] ;
}model_recv_infor_factory_t;

/* data of custom ack state model*/
typedef struct 
{
   uint8_t stateinGW;
   uint16_t addr;
}model_ack_state_t;


void print_data_send (void);
#endif // __CUSTOM_SENSOR_MODEL_DEFS_H__