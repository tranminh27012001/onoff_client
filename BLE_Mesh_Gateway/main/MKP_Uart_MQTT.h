#ifndef _MKP_UART_
#define _MKP_UART_

#include <stdint.h>
#include "cJSON.h"

#define MES_QUEUE_LEN 10
#define MAX_LEN_MSG 400
#define MAX_MQTT_TOPIC_LEN 80
#define MAX_MQTT_DATA_LEN 150

#define EVT_CONTROL_PROPERTY "CP"

#define LOCAL_ID_SERVER_TO_DEVICE(id) (id-1)
#define LOCAL_ID_DEVICE_TO_SERVER(id) (id+1)

typedef struct 
{
    char topic[MAX_MQTT_TOPIC_LEN];
    char data[MAX_MQTT_DATA_LEN];
}mqttMes_t;

/*----------------------*/
void PROTO_init() ;
void PROTO_processCmd(char *data);
void UART_TO_MQTT_PublishNewState(char *g_product_Id ,const char *proCode, uint8_t localId, int16_t state, char *mid, char *reason, cJSON *info) ;

#endif