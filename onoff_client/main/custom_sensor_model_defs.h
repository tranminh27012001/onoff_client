
#ifndef __CUSTOM_SENSOR_MODEL_DEFS_H__
#define __CUSTOM_SENSOR_MODEL_DEFS_H__

#include <stdio.h>

#include "sdkconfig.h"

#include "esp_ble_mesh_common_api.h"

// Config Name
#define CONFIG_MESH_DEVICE_NAME "GATE WAY"
#define BLE_MESH_DEVICE_NAME CONFIG_MESH_DEVICE_NAME
#define CID_ESP 0x02E5

// define MOdel_id sensor custom
#define ESP_BLE_MESH_CUSTOM_SENSOR_MODEL_ID_SERVER 0x1414
#define ESP_BLE_MESH_CUSTOM_SENSOR_MODEL_ID_CLIENT 0x1415
// define Model control custom
#define ESP_BLE_MESH_CUSTOM_CONTROL_MODEL_ID_SERVER 0x1416
#define ESP_BLE_MESH_CUSTOM_CONTROL_MODEL_ID_CLIENT 0x1417
//* Definimos os OPCODES das mensagens (igual no server)
#define ESP_BLE_MESH_CUSTOM_SENSOR_MODEL_OP_GET     ESP_BLE_MESH_MODEL_OP_3(0x00, CID_ESP)
#define ESP_BLE_MESH_CUSTOM_SENSOR_MODEL_OP_SET     ESP_BLE_MESH_MODEL_OP_3(0x01, CID_ESP)
#define ESP_BLE_MESH_CUSTOM_SENSOR_MODEL_OP_STATUS   ESP_BLE_MESH_MODEL_OP_3(0x02, CID_ESP)
#define ESP_BLE_MESH_CUSTOM_CONTROL_MODEL_OP_GET    ESP_BLE_MESH_MODEL_OP_3(0x03, CID_ESP)
#define ESP_BLE_MESH_CUSTOM_CONTROL_MODEL_OP_SET     ESP_BLE_MESH_MODEL_OP_3(0x04, CID_ESP)
#define ESP_BLE_MESH_CUSTOM_CONTROL_MODEL_OP_STATUS  ESP_BLE_MESH_MODEL_OP_3(0x05, CID_ESP)

#define ESP_BLE_MESH_GROUP_PUB_ADDR 0xC100

typedef struct
{
    uint16_t addrr[5];
    uint16_t scene;
    uint8_t state[5];
} data_scene_onoff_t;

typedef struct __attribute__((packed))
{
    uint8_t cmdID;
    data_scene_onoff_t data_scene_onoff ;

    uint8_t start_stop;
} model_sensor_data_t;



typedef struct __attribute__((packed))
{
   uint16_t element_control_adrr ;
   uint16_t scene ;
}model_control_data_t;


void print_data_send (void);
#endif // __CUSTOM_SENSOR_MODEL_DEFS_H__