/* main.c - Application main entry point */

/*
 * Copyright (c) 2017 Intel Corporation
 * Additional Copyright (c) 2018 Espressif Systems (Shanghai) PTE LTD
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#include "esp_log.h"
#include "nvs_flash.h"
#include <sdkconfig.h>

#include "esp_ble_mesh_common_api.h"
#include "esp_ble_mesh_provisioning_api.h"
#include "esp_ble_mesh_networking_api.h"
#include "esp_ble_mesh_config_model_api.h"
#include "esp_ble_mesh_generic_model_api.h"
#include "esp_ble_mesh_defs.h"
#include "esp_ble_mesh_time_scene_model_api.h"

#include "ble_mesh_example_init.h"
#include "ble_mesh_example_nvs.h"

#include "esp_bt.h"
#include "esp_bt_main.h"
#include "esp_bt_device.h"

#include "mesh_buf.h"
#include "custom_sensor_model_defs.h"



/*===========================================DEFINE=====================================================================*/
#define TAG "EXAMPLE"
#define ELEMNET_COUNT 10
#define SIZE_OF_ARR(arr) (sizeof(arr) / sizeof(arr[0]))

#define CID_ESP 0x02E5

/*========================================DECLARE VARIBALE=============================================================*/
static uint8_t dev_uuid[16] = {0xdd, 0xdd};
static nvs_handle_t NVS_HANDLE;
static const char *NVS_KEY = "onoff_client";
typedef struct
{
    uint16_t Element_addr;
    uint8_t Element_state;
} Element_inf_t;
static Element_inf_t element_inf[ELEMNET_COUNT];

static struct example_info_store
{
    uint16_t net_idx; /* NetKey Index */
    uint16_t app_idx; /* AppKey Index */
    uint8_t onoff;    /* Remote OnOff */
    uint8_t tid;      /* Message TID */
} __attribute__((packed)) store = {
    .net_idx = ESP_BLE_MESH_KEY_UNUSED,
    .app_idx = ESP_BLE_MESH_KEY_UNUSED,
    .onoff = 0,
    .tid = 0x0,
};

/*=========================================STRART CODING==========================================================*/

/*-------config server -----------*/
static esp_ble_mesh_cfg_srv_t config_server = {
    .relay = ESP_BLE_MESH_RELAY_ENABLED,
    .beacon = ESP_BLE_MESH_BEACON_ENABLED,
#if defined(CONFIG_BLE_MESH_FRIEND)
    .friend_state = ESP_BLE_MESH_FRIEND_ENABLED,
#else
    .friend_state = ESP_BLE_MESH_FRIEND_NOT_SUPPORTED,
#endif
#if defined(CONFIG_BLE_MESH_GATT_PROXY_SERVER)
    .gatt_proxy = ESP_BLE_MESH_GATT_PROXY_ENABLED,
#else
    .gatt_proxy = ESP_BLE_MESH_GATT_PROXY_NOT_SUPPORTED,
#endif
    .default_ttl = 7,
    /* 3 transmissions with 20ms interval */
    .net_transmit = ESP_BLE_MESH_TRANSMIT(2, 20),
    .relay_retransmit = ESP_BLE_MESH_TRANSMIT(2, 20),
};


/* -----set up onoff client model --------------*/
static esp_ble_mesh_client_t onoff_client;
ESP_BLE_MESH_MODEL_PUB_DEFINE(onoff_cli_pub, 2 + 1, ROLE_NODE);


/*------set up scene client--------------------*/
static esp_ble_mesh_client_t scene_client;

ESP_BLE_MESH_MODEL_PUB_DEFINE(scene_cli_pub, 2 + 1, ROLE_NODE);

/*------set up custom set scene onoff model--------------*/

static const esp_ble_mesh_client_op_pair_t custom_model_op_pair[] = {
    {ESP_BLE_MESH_CUSTOM_SENSOR_MODEL_OP_GET , ESP_BLE_MESH_CUSTOM_SENSOR_MODEL_OP_STATUS },
};


static esp_ble_mesh_model_op_t custom_sensor_op[] = {
    ESP_BLE_MESH_MODEL_OP(ESP_BLE_MESH_CUSTOM_SENSOR_MODEL_OP_STATUS, 2),
    ESP_BLE_MESH_MODEL_OP_END,
};


static esp_ble_mesh_client_t custom_sensor_client = {
    .op_pair_size = ARRAY_SIZE(custom_model_op_pair),
    .op_pair = custom_model_op_pair,
};

/*-----------set up custom control model---------------*/
static const esp_ble_mesh_client_op_pair_t custom_model_crt_op_pair[] = {
    {ESP_BLE_MESH_CUSTOM_CONTROL_MODEL_OP_GET , ESP_BLE_MESH_CUSTOM_CONTROL_MODEL_OP_STATUS },
};


static esp_ble_mesh_model_op_t custom_model_ctr_op[] = {
    ESP_BLE_MESH_MODEL_OP(ESP_BLE_MESH_CUSTOM_CONTROL_MODEL_OP_STATUS, 2),
    ESP_BLE_MESH_MODEL_OP_END,
};


static esp_ble_mesh_client_t custom_model_ctr_client = {
    .op_pair_size = ARRAY_SIZE(custom_model_crt_op_pair),
    .op_pair = custom_model_crt_op_pair,
};

static esp_ble_mesh_model_t custom_models[] = {
    ESP_BLE_MESH_VENDOR_MODEL(CID_ESP, ESP_BLE_MESH_CUSTOM_SENSOR_MODEL_ID_CLIENT,
    custom_sensor_op, NULL, &custom_sensor_client),
    ESP_BLE_MESH_VENDOR_MODEL(CID_ESP, ESP_BLE_MESH_CUSTOM_CONTROL_MODEL_ID_CLIENT,
    custom_model_ctr_op, NULL, &custom_model_ctr_client),
};

/*-------gan model cho element----------*/


static esp_ble_mesh_model_t root_models[] = {
    ESP_BLE_MESH_MODEL_CFG_SRV(&config_server),
    ESP_BLE_MESH_MODEL_GEN_ONOFF_CLI(&onoff_cli_pub, &onoff_client),
    ESP_BLE_MESH_MODEL_SCENE_CLI(&scene_cli_pub, &scene_client),
};
/*------link model -----------------*/
static esp_ble_mesh_elem_t elements[] = {
    ESP_BLE_MESH_ELEMENT(0, root_models, custom_models),
};

static esp_ble_mesh_comp_t composition = {
    .cid = CID_ESP,
    .elements = elements,
    .element_count = ARRAY_SIZE(elements),
};

/* Disable OOB security for SILabs Android app */
static esp_ble_mesh_prov_t provision = {
    .uuid = dev_uuid,
    .output_size = 0,
    .output_actions = 0,
};
/*################$$$$$$$$$$$$$$$$$$$$###### KHAI BAO HAM#######################################################*/

static void mesh_example_info_store(void);
static void mesh_example_info_restore(void);
void example_ble_mesh_send_gen_onoff_set(uint16_t addr, uint8_t state);


void example_ble_mesh_send_gen_onoff_get(uint16_t addr);
void handle_client_scene_set_store( uint16_t scene_number_);
void handle_client_scene_set_ReCall ( uint16_t scene_number_) ;
void show_arr_state(void) ;

void tx_power_set (void);
void state_storage(uint16_t addr, uint8_t state);
static void parse_received_data(esp_ble_mesh_model_cb_param_t *recv_param, model_sensor_data_t *parsed_data);
static void parse_rev_data_model_ctr(esp_ble_mesh_model_cb_param_t *recv_param, model_control_data_t *parsed_data);
esp_err_t ble_mesh_custom_sensor_client_model_message_set(model_sensor_data_t set_data) ;
esp_err_t ble_mesh_custom_elm_control_client_model_message_set(model_control_data_t set_data);
esp_err_t ble_mesh_custom_sensor_client_model_message_get(void) ;

/*######################################## FUNCION CALLBACK ####################################################*/

static void prov_complete(uint16_t net_idx, uint16_t addr, uint8_t flags, uint32_t iv_index)
{
    ESP_LOGI(TAG, "net_idx: 0x%04x, addr: 0x%04x", net_idx, addr);
    ESP_LOGI(TAG, "flags: 0x%02x, iv_index: 0x%08x", flags, iv_index);
    store.net_idx = net_idx;
}


static void example_ble_mesh_provisioning_cb(esp_ble_mesh_prov_cb_event_t event,
                                             esp_ble_mesh_prov_cb_param_t *param)
{
    switch (event)
    {
    case ESP_BLE_MESH_PROV_REGISTER_COMP_EVT:
        ESP_LOGI(TAG, "ESP_BLE_MESH_PROV_REGISTER_COMP_EVT, err_code %d", param->prov_register_comp.err_code);
        mesh_example_info_restore(); /* Restore proper mesh example info */
        break;
    case ESP_BLE_MESH_NODE_PROV_ENABLE_COMP_EVT:
        ESP_LOGI(TAG, "ESP_BLE_MESH_NODE_PROV_ENABLE_COMP_EVT, err_code %d", param->node_prov_enable_comp.err_code);
        break;
    case ESP_BLE_MESH_NODE_PROV_LINK_OPEN_EVT:
        ESP_LOGI(TAG, "ESP_BLE_MESH_NODE_PROV_LINK_OPEN_EVT, bearer %s",
                 param->node_prov_link_open.bearer == ESP_BLE_MESH_PROV_ADV ? "PB-ADV" : "PB-GATT");
        break;
    case ESP_BLE_MESH_NODE_PROV_LINK_CLOSE_EVT:
        ESP_LOGI(TAG, "ESP_BLE_MESH_NODE_PROV_LINK_CLOSE_EVT, bearer %s",
                 param->node_prov_link_close.bearer == ESP_BLE_MESH_PROV_ADV ? "PB-ADV" : "PB-GATT");
        break;
    case ESP_BLE_MESH_NODE_PROV_COMPLETE_EVT:
        ESP_LOGI(TAG, "ESP_BLE_MESH_NODE_PROV_COMPLETE_EVT");
        prov_complete(param->node_prov_complete.net_idx, param->node_prov_complete.addr,
                      param->node_prov_complete.flags, param->node_prov_complete.iv_index);
        break;
    case ESP_BLE_MESH_NODE_PROV_RESET_EVT:
        break;
    case ESP_BLE_MESH_NODE_SET_UNPROV_DEV_NAME_COMP_EVT:
        ESP_LOGI(TAG, "ESP_BLE_MESH_NODE_SET_UNPROV_DEV_NAME_COMP_EVT, err_code %d", param->node_set_unprov_dev_name_comp.err_code);
        break;
    default:
        break;
    }
}

static void example_ble_mesh_config_server_cb(esp_ble_mesh_cfg_server_cb_event_t event,
                                              esp_ble_mesh_cfg_server_cb_param_t *param)
{
    if (event == ESP_BLE_MESH_CFG_SERVER_STATE_CHANGE_EVT)
    {
        switch (param->ctx.recv_op)
        {
        case ESP_BLE_MESH_MODEL_OP_APP_KEY_ADD:
            ESP_LOGI(TAG, "ESP_BLE_MESH_MODEL_OP_APP_KEY_ADD");
            ESP_LOGI(TAG, "net_idx 0x%04x, app_idx 0x%04x",
                     param->value.state_change.appkey_add.net_idx,
                     param->value.state_change.appkey_add.app_idx);
            ESP_LOG_BUFFER_HEX("AppKey", param->value.state_change.appkey_add.app_key, 16);
            break;
        case ESP_BLE_MESH_MODEL_OP_MODEL_APP_BIND:
            ESP_LOGI(TAG, "ESP_BLE_MESH_MODEL_OP_MODEL_APP_BIND");
            ESP_LOGI(TAG, "elem_addr 0x%04x, app_idx 0x%04x, cid 0x%04x, mod_id 0x%04x",
                     param->value.state_change.mod_app_bind.element_addr,
                     param->value.state_change.mod_app_bind.app_idx,
                     param->value.state_change.mod_app_bind.company_id,
                     param->value.state_change.mod_app_bind.model_id);
          
                store.app_idx = param->value.state_change.mod_app_bind.app_idx;
                mesh_example_info_store(); /* Store proper mesh example info */

            break;
        default:
            break;
        }
    }
}

static void example_ble_mesh_generic_client_cb(esp_ble_mesh_generic_client_cb_event_t event,
                                               esp_ble_mesh_generic_client_cb_param_t *param)
{
    ESP_LOGI(TAG, "Generic client, event %u, error code %d, opcode is 0x%04x",
             event, param->error_code, param->params->opcode);

    switch (event)
    {
    case ESP_BLE_MESH_GENERIC_CLIENT_GET_STATE_EVT:
        ESP_LOGI(TAG, "ESP_BLE_MESH_GENERIC_CLIENT_GET_STATE_EVT");
        if (param->params->opcode == ESP_BLE_MESH_MODEL_OP_GEN_ONOFF_GET)
        {
            ESP_LOGI(TAG, "ESP_BLE_MESH_MODEL_OP_GEN_ONOFF_GET, onoff %d", param->status_cb.onoff_status.present_onoff);
            // state_storage(param->params->ctx.addr, param->status_cb.onoff_status.present_onoff);
            
        }
        break;
    case ESP_BLE_MESH_GENERIC_CLIENT_SET_STATE_EVT:
        ESP_LOGI(TAG, "ESP_BLE_MESH_GENERIC_CLIENT_SET_STATE_EVT");
        if (param->params->opcode == ESP_BLE_MESH_MODEL_OP_GEN_ONOFF_SET)
        {
            ESP_LOGI(TAG, "ESP_BLE_MESH_MODEL_OP_GEN_ONOFF_SET, onoff %d", param->status_cb.onoff_status.present_onoff);
        }
        break;
    case ESP_BLE_MESH_GENERIC_CLIENT_PUBLISH_EVT:
        ESP_LOGI(TAG, "ESP_BLE_MESH_GENERIC_CLIENT_PUBLISH_EVT,....addr %d, ..... status %d", param->params->ctx.addr, param->status_cb.onoff_status.present_onoff);
        state_storage(param->params->ctx.addr, param->status_cb.onoff_status.present_onoff);
        break;
    case ESP_BLE_MESH_GENERIC_CLIENT_TIMEOUT_EVT:
        ESP_LOGI(TAG, "ESP_BLE_MESH_GENERIC_CLIENT_TIMEOUT_EVT");
        break;
    default:
        break;
    }
}
static void ble_mesh_client_scene_cb(esp_ble_mesh_time_scene_client_cb_event_t event, esp_ble_mesh_time_scene_client_cb_param_t *param)
{

    switch (event)
    {
    case ESP_BLE_MESH_TIME_SCENE_CLIENT_SET_STATE_EVT:
        ESP_LOGI(TAG, " ESP_BLE_MESH_TIME_SCENE_CLIENT_SET_STATE_EVT ");
        break;
    case ESP_BLE_MESH_TIME_SCENE_CLIENT_PUBLISH_EVT:
        ESP_LOGI(TAG, "ESP_BLE_MESH_TIME_SCENE_CLIENT_PUBLISH_EVT ");
        break;
    case ESP_BLE_MESH_TIME_SCENE_CLIENT_TIMEOUT_EVT:
        ESP_LOGI(TAG, "TIME OUT ROI ");
        break;

    default:
        break;
    }
}


static void ble_mesh_custom_sensor_client_model_cb(esp_ble_mesh_model_cb_event_t event,
                                                   esp_ble_mesh_model_cb_param_t *param) {
    switch (event) {
        case ESP_BLE_MESH_MODEL_OPERATION_EVT:
            switch (param->model_operation.opcode) {
                case ESP_BLE_MESH_CUSTOM_SENSOR_MODEL_OP_STATUS:
                    ESP_LOGI(TAG, "OP_STATUS : 0x%06x", param->model_operation.opcode);
                    ESP_LOG_BUFFER_HEX(TAG, param->model_operation.msg, param->model_operation.length);
                break;

                case ESP_BLE_MESH_CUSTOM_CONTROL_MODEL_OP_STATUS :
                    ESP_LOGI(TAG, "OP_STATUS : 0x%06x", param->model_operation.opcode);
                    ESP_LOG_BUFFER_HEX(TAG, param->model_operation.msg, param->model_operation.length);
                 break;

                default:
                    ESP_LOGW(TAG, "Received unrecognized OPCODE message");
                break;
            }
        break;

        case ESP_BLE_MESH_MODEL_SEND_COMP_EVT:
            if (param->model_send_comp.err_code) {
                ESP_LOGE(TAG, "ESP_BLE_MESH_MODEL_SEND_COMP_EVT OPCODE 0x%06x", param->model_send_comp.opcode);
                break;
            }
            ESP_LOGI(TAG, "ESP_BLE_MESH_MODEL_SEND_COMP_EVT OPCODE 0x%06x ", param->model_send_comp.opcode);

        break;

        case ESP_BLE_MESH_CLIENT_MODEL_RECV_PUBLISH_MSG_EVT:
            switch (param->client_recv_publish_msg.opcode) {
                case ESP_BLE_MESH_CUSTOM_SENSOR_MODEL_OP_STATUS:
                    ESP_LOGI(TAG, "OP_STATUS -- Mensagem recebida: 0x%06x", param->client_recv_publish_msg.opcode);
                    ESP_LOG_BUFFER_HEX(TAG, param->client_recv_publish_msg.msg, param->client_recv_publish_msg.length);

                    model_sensor_data_t received_data;
                    parse_received_data(param, &received_data);
                break;

                case ESP_BLE_MESH_CUSTOM_CONTROL_MODEL_OP_STATUS:
                   ESP_LOGI(TAG, " DATA PUB FROM SRV");

                   model_control_data_t received_ctrl_data;
                    parse_rev_data_model_ctr(param, &received_ctrl_data);
                 break;
                default:
                    ESP_LOGW(TAG, "Received OPCODE message: 0x%04x", param->client_recv_publish_msg.opcode);
                break;
            }


        break;

        case ESP_BLE_MESH_CLIENT_MODEL_SEND_TIMEOUT_EVT:
            ESP_LOGW(TAG, "Mensagem 0x%06x timeout", param->client_send_timeout.opcode);
           
        break;

        default:
            ESP_LOGW(TAG, "%s - Unrecognized event: 0x%04x", __func__, event);
        break;
    }
}


/*############################################ FUNCION HANDLE ###################################################*/

static void mesh_example_info_store(void)
{
    ble_mesh_nvs_store(NVS_HANDLE, NVS_KEY, &store, sizeof(store));
}

static void mesh_example_info_restore(void)
{
    esp_err_t err = ESP_OK;
    bool exist = false;

    err = ble_mesh_nvs_restore(NVS_HANDLE, NVS_KEY, &store, sizeof(store), &exist);
    if (err != ESP_OK)
    {
        return;
    }

    if (exist)
    {
        ESP_LOGI(TAG, "Restore, net_idx 0x%04x, app_idx 0x%04x, onoff %u, tid 0x%02x",
                 store.net_idx, store.app_idx, store.onoff, store.tid);
    }
}




void example_ble_mesh_send_gen_onoff_set(uint16_t addr, uint8_t state)
{
    esp_ble_mesh_generic_client_set_state_t set = {0};
    esp_ble_mesh_client_common_param_t common = {0};
    esp_err_t err = ESP_OK;

    common.opcode = ESP_BLE_MESH_MODEL_OP_GEN_ONOFF_SET_UNACK;
    common.model = onoff_client.model;
    common.ctx.net_idx = store.net_idx;
    common.ctx.app_idx = store.app_idx;
    common.ctx.addr = addr; /* to all nodes */
    common.ctx.send_ttl = 3;
    common.ctx.send_rel = false;
    common.msg_timeout = 0; /* 0 indicates that timeout value from menuconfig will be used */
    common.msg_role = ROLE_NODE;

    set.onoff_set.op_en = false;
    set.onoff_set.onoff = state;
    set.onoff_set.tid = store.tid++;

    err = esp_ble_mesh_generic_client_set_state(&common, &set);
    if (err)
    {
        ESP_LOGE(TAG, "Send Generic OnOff Set Unack failed");
        return;
    }

    store.onoff = state;
    mesh_example_info_store(); /* Store proper mesh example info */
}



void example_ble_mesh_send_gen_onoff_get(uint16_t addr)
{
    esp_ble_mesh_generic_client_get_state_t get = {0};
    esp_ble_mesh_client_common_param_t common = {0};
    esp_err_t err = ESP_OK;

    common.opcode = ESP_BLE_MESH_MODEL_OP_GEN_ONOFF_GET;
    common.model = onoff_client.model;
    common.ctx.net_idx = store.net_idx;
    common.ctx.app_idx = store.app_idx;
    common.ctx.addr = addr; /* to all nodes */
    common.ctx.send_ttl = 3;
    common.ctx.send_rel = false;
    common.msg_timeout = 0; /* 0 indicates that timeout value from menuconfig will be used */
    common.msg_role = ROLE_NODE;


    err = esp_ble_mesh_generic_client_get_state(&common, &get);
    if (err)
    {
        ESP_LOGE(TAG, "Send Generic OnOff Set Unack failed");
        return;
    }

}
void handle_client_scene_set_store( uint16_t scene_number_)      // Hàm tạo scene
{
    esp_ble_mesh_client_common_param_t common={0};
    esp_ble_mesh_time_scene_client_set_state_t set={0};
    common.model = scene_client.model;
    common.opcode = ESP_BLE_MESH_MODEL_OP_SCENE_STORE;
    common.ctx.net_idx = store.net_idx;
    common.ctx.app_idx = store.app_idx;
    common.ctx.addr = 0xFFFF;
    common.ctx.send_ttl = 3;
    common.ctx.send_rel = false;
    common.msg_timeout = 0;
    common.msg_role = ROLE_NODE;

   
    set.scene_store.scene_number = scene_number_;
    

    esp_ble_mesh_time_scene_client_set_state(&common, &set);
     printf("done storeeee");
}


void handle_client_scene_set_ReCall ( uint16_t scene_number_)               // Hàm gọi ReCall
{   
    esp_ble_mesh_client_common_param_t common= {0} ;
    esp_ble_mesh_time_scene_client_set_state_t set ={0}  ;
    common.model = scene_client.model ;
    common.opcode = ESP_BLE_MESH_MODEL_OP_SCENE_RECALL_UNACK ;
    
    common.ctx.net_idx = store.net_idx;
    common.ctx.app_idx = store.app_idx;
    common.ctx.addr = 0xFFFF; 
    common.ctx.send_ttl = 3;
    common.ctx.send_rel = false;
    common.msg_timeout = 0; 
    common.msg_role = ROLE_NODE;
    
    set.scene_recall.op_en = false;
    set.scene_recall.scene_number= scene_number_;
    set.scene_recall.tid = store.tid++ ;
    
     esp_ble_mesh_time_scene_client_set_state(&common, &set);
     printf("enddddddddd handle client set scene");
} 

void show_arr_state(void)
{
    for (uint8_t i = 0; i < SIZE_OF_ARR(element_inf); i++)
    {
        printf("state %d, addr %d \n", element_inf[i].Element_state, element_inf[i].Element_addr);
    }
}
void state_storage(uint16_t addr, uint8_t state)
{
    for (uint8_t i = 0; i < SIZE_OF_ARR(element_inf); i++)
    {
        if (addr == element_inf[i].Element_addr)
        {
            element_inf[i].Element_state = state;
            break;
        }

        if (element_inf[i].Element_addr == 0)

        {
            element_inf[i].Element_addr = addr;
            element_inf[i].Element_state = state;
            break;
        }
    }
}
void tx_power_set (void)
{   
    esp_ble_tx_power_set(ESP_BLE_PWR_TYPE_CONN_HDL0, ESP_PWR_LVL_P9);
    esp_ble_tx_power_set(ESP_BLE_PWR_TYPE_CONN_HDL1, ESP_PWR_LVL_P9);
    esp_ble_tx_power_set(ESP_BLE_PWR_TYPE_CONN_HDL2, ESP_PWR_LVL_P9);
    esp_ble_tx_power_set(ESP_BLE_PWR_TYPE_CONN_HDL3, ESP_PWR_LVL_P9);
    esp_ble_tx_power_set(ESP_BLE_PWR_TYPE_CONN_HDL4, ESP_PWR_LVL_P9);
    esp_ble_tx_power_set(ESP_BLE_PWR_TYPE_CONN_HDL5, ESP_PWR_LVL_P9);
    esp_ble_tx_power_set(ESP_BLE_PWR_TYPE_CONN_HDL6, ESP_PWR_LVL_P9);
    esp_ble_tx_power_set(ESP_BLE_PWR_TYPE_CONN_HDL7, ESP_PWR_LVL_P9);
    esp_ble_tx_power_set(ESP_BLE_PWR_TYPE_CONN_HDL8, ESP_PWR_LVL_P9);
    esp_ble_tx_power_set(ESP_BLE_PWR_TYPE_ADV, ESP_PWR_LVL_P9);
    esp_ble_tx_power_set(ESP_BLE_PWR_TYPE_SCAN, ESP_PWR_LVL_P9);
}


static void parse_received_data(esp_ble_mesh_model_cb_param_t *recv_param, model_sensor_data_t *parsed_data) {
    if (recv_param->client_recv_publish_msg.length < sizeof(parsed_data)) {
        ESP_LOGE(TAG, "Invalid received message lenght: %d", recv_param->client_recv_publish_msg.length);
        return;
    }

    parsed_data = (model_sensor_data_t *)recv_param->client_recv_publish_msg.msg;
}

static void parse_rev_data_model_ctr(esp_ble_mesh_model_cb_param_t *recv_param, model_control_data_t *parsed_data)
{
    if(recv_param->client_recv_publish_msg.length< sizeof(parsed_data)){
        ESP_LOGE(TAG, "Invalid received message lenght: %d", recv_param->client_recv_publish_msg.length);
        return;
    }
    parsed_data = (model_control_data_t *)recv_param->client_recv_publish_msg.msg;
}


esp_err_t ble_mesh_custom_sensor_client_model_message_set(model_sensor_data_t set_data) {
    esp_ble_mesh_msg_ctx_t ctx = {0};
    uint32_t opcode;
    esp_err_t err;

    opcode = ESP_BLE_MESH_CUSTOM_SENSOR_MODEL_OP_SET;
    
   ctx.net_idx = store.net_idx;
    ctx.app_idx = store.app_idx;
    ctx.addr = ESP_BLE_MESH_ADDR_ALL_NODES;
    // ctx.addr = 0xFFFF;
    ctx.send_ttl = 3;
    ctx.send_rel = false;

    err = esp_ble_mesh_client_model_send_msg(custom_sensor_client.model, &ctx, opcode,
            sizeof(set_data), (uint8_t *)&set_data, 0, false, ROLE_NODE);

    if (err != ESP_OK) {
        ESP_LOGE(TAG, "Falha ao enviar a msg custom 0x%06x, err = 0x%06x", opcode, err);
    }

    return err;
}

esp_err_t ble_mesh_custom_elm_control_client_model_message_set(model_control_data_t set_data) {
    esp_ble_mesh_msg_ctx_t ctx = {0};
    uint32_t opcode;
    esp_err_t err;

    opcode = ESP_BLE_MESH_CUSTOM_CONTROL_MODEL_OP_SET;
    
    ctx.net_idx = store.net_idx;
    ctx.app_idx = store.app_idx;
    // ctx.addr = ESP_BLE_MESH_ADDR_ALL_NODES;
    ctx.addr = 0xFFFF;
    ctx.send_ttl = 3;
    ctx.send_rel = false;

    err = esp_ble_mesh_client_model_send_msg(custom_model_ctr_client.model, &ctx, opcode,
            sizeof(set_data), (uint8_t *)&set_data, 0, false, ROLE_NODE);

    if (err != ESP_OK) {
        ESP_LOGE(TAG, "Falha ao enviar a msg custom 0x%06x, err = 0x%06x", opcode, err);
    }

    return err;
}
esp_err_t ble_mesh_custom_sensor_client_model_message_get(void) {
    esp_ble_mesh_msg_ctx_t ctx = {0};
    uint32_t opcode;
    esp_err_t err;

    opcode = ESP_BLE_MESH_CUSTOM_SENSOR_MODEL_OP_GET;

    ctx.net_idx = store.net_idx;
    ctx.app_idx = store.app_idx;
    // ctx.addr = ESP_BLE_MESH_ADDR_ALL_NODES;
    ctx.addr = 0xFFFF;  //! FIXME: passar o endereco do device pra GET?
    ctx.send_ttl = 3;
    ctx.send_rel = false;

    ESP_LOGI(TAG, "*** %s ***", __func__);
    ESP_LOGI(TAG, "\t app_idx: 0x%04x, net_idx: 0x%04x, dest_addr: 0x%04x, opcode: 0x%04x", 
                store.app_idx, store.net_idx, ctx.addr, opcode);

    err = esp_ble_mesh_client_model_send_msg(custom_sensor_client.model, &ctx, opcode,
            0, NULL, 0, true, ROLE_NODE);

    if (err != ESP_OK) {
        ESP_LOGE(TAG, "FALSE 0x%06x, err = 0x%06x", opcode, err);
    }

    return err;
}

/**********************************************************************************************************************************************************/

static esp_err_t ble_mesh_init(void)
{
    esp_err_t err = ESP_OK;

    esp_ble_mesh_register_prov_callback(example_ble_mesh_provisioning_cb);
    esp_ble_mesh_register_generic_client_callback(example_ble_mesh_generic_client_cb);
    esp_ble_mesh_register_config_server_callback(example_ble_mesh_config_server_cb);
    esp_ble_mesh_register_time_scene_client_callback(ble_mesh_client_scene_cb);
    esp_ble_mesh_register_custom_model_callback(ble_mesh_custom_sensor_client_model_cb);

    err = esp_ble_mesh_init(&provision, &composition);
    if (err != ESP_OK)
    {
        ESP_LOGE(TAG, "Failed to initialize mesh stack (err %d)", err);
        return err;
    }
    err = esp_ble_mesh_client_model_init(&custom_models[0]);
    if (err) {
        ESP_LOGE(TAG, "Failed to initialize vendor client");
        return err;
    }
    err = esp_ble_mesh_client_model_init(&custom_models[1]);
     if (err) {
        ESP_LOGE(TAG, "Failed to initialize vendor client");
        return err;
    }
    esp_ble_mesh_set_unprovisioned_device_name(BLE_MESH_DEVICE_NAME);
    err = esp_ble_mesh_node_prov_enable(ESP_BLE_MESH_PROV_ADV | ESP_BLE_MESH_PROV_GATT);
    if (err != ESP_OK)
    {
        ESP_LOGE(TAG, "Failed to enable mesh node (err %d)", err);
        return err;
    }

    ESP_LOGI(TAG, "BLE Mesh Node initialized");

    return err;
}
void set_scene_and_elm_control(void)
{       model_control_data_t set_data_control ;
        set_data_control.element_control_adrr =0x009A;
         set_data_control.scene =0x0001 ;
         model_sensor_data_t set_data_scene ;
         set_data_scene.data_scene_onoff.addrr[0]= 0x009C ;
        set_data_scene.data_scene_onoff.state[0]=1; 
        set_data_scene.data_scene_onoff.addrr[1]= 0x009D ;
        set_data_scene.data_scene_onoff.state[1]=0; 
        set_data_scene.data_scene_onoff.scene =0x0001;
        handle_client_scene_set_store (0x0001);
        vTaskDelay(500/portTICK_RATE_MS);
        ble_mesh_custom_elm_control_client_model_message_set(set_data_control);
        vTaskDelay(500/portTICK_RATE_MS);
        ble_mesh_custom_sensor_client_model_message_set(set_data_scene);


}

void app_main(void)
{
    esp_err_t err;
    tx_power_set();

    err = nvs_flash_init();
    if (err == ESP_ERR_NVS_NO_FREE_PAGES)
    {
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
    }
    ESP_ERROR_CHECK(err);

    err = bluetooth_init();
    if (err)
    {
        ESP_LOGE(TAG, "esp32_bluetooth_init failed (err %d)", err);
        return;
    }
    ble_mesh_nvs_open(&NVS_HANDLE);
    ble_mesh_get_dev_uuid(dev_uuid);
    ble_mesh_init();

    set_scene_and_elm_control();
    // while(1)
    // {    
    //  handle_client_scene_set_ReCall(0x0001);
    //  printf("\n ket thuc");
    // vTaskDelay(000/portTICK_RATE_MS);
    // }
     
}
    

