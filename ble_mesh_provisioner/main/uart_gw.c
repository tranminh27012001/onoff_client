#include <stdio.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "driver/uart.h"
#include "esp_log.h"

#include "custom_model_defs.h"
#include "model_hanlde.h"
#include "uart_gw.h"





static const char *TAG = "uart_events";



#define EX_UART_NUM UART_NUM_0
#define PATTERN_CHR_NUM    (3)         /*!< Set the number of consecutive and identical characters received by receiver which defines a UART pattern*/

#define BUF_SIZE (1024)
#define RD_BUF_SIZE (BUF_SIZE)
static QueueHandle_t uart0_queue;

bool is_cread_scene_numb = false ;



uint8_t Cks_Data (uint8_t *rxBuff, uint8_t leng)
{
    uint8_t temp = rxBuff[2];
    for(uint8_t i=3; i< (leng-2); i++ )
    {
        temp =temp ^ rxBuff[i];
    }
    return temp ;
}
void ESP_Recv_Data (uint8_t ID, uint8_t* data_recv);
   
static void uart_event_task(void *pvParameters)
{
    uart_event_t event;
    size_t buffered_size;
    uint8_t* dtmp = (uint8_t*) malloc(RD_BUF_SIZE);
     uint8_t Buff_temp[7];
    for(;;) {
        //Waiting for UART event.
        if(xQueueReceive(uart0_queue, (void * )&event, (portTickType)portMAX_DELAY)) {
            bzero(dtmp, RD_BUF_SIZE);
            // ESP_LOGI(TAG, "uart[%d] event:", EX_UART_NUM);
            switch(event.type) {
          
                case UART_DATA:
                   
                    uart_read_bytes(EX_UART_NUM, dtmp, event.size, portMAX_DELAY);
                     uint8_t checksum = Cks_Data(dtmp , (uint8_t)event.size);
                        if(dtmp[0]== BYTE_START && dtmp[event.size-1] == BYTE_STOP &&  checksum == dtmp[event.size-2])
                        {   
                           
                            memcpy(Buff_temp , dtmp+2, event.size-4);
                            ESP_Recv_Data(dtmp[1], Buff_temp );
                            
                        }
       

                    // uart_write_bytes(EX_UART_NUM, (const char*) dtmp, event.size);
                    break;
                //Event of HW FIFO overflow detected
                case UART_FIFO_OVF:
                    ESP_LOGI(TAG, "hw fifo overflow");
               
                    uart_flush_input(EX_UART_NUM);
                    xQueueReset(uart0_queue);
                    break;
            
                case UART_BUFFER_FULL:
                    ESP_LOGI(TAG, "ring buffer full");
                 
                    uart_flush_input(EX_UART_NUM);
                    xQueueReset(uart0_queue);
                    break;
              
                case UART_BREAK:
                    ESP_LOGI(TAG, "uart rx break");
                    break;
              
                case UART_PARITY_ERR:
                    ESP_LOGI(TAG, "uart parity error");
                    break;
          
                case UART_FRAME_ERR:
                    ESP_LOGI(TAG, "uart frame error");
                    break;
           
                case UART_PATTERN_DET:
                    uart_get_buffered_data_len(EX_UART_NUM, &buffered_size);
                    int pos = uart_pattern_pop_pos(EX_UART_NUM);
                    ESP_LOGI(TAG, "[UART PATTERN DETECTED] pos: %d, buffered size: %d", pos, buffered_size);
                    if (pos == -1) {
                     
                        uart_flush_input(EX_UART_NUM);
                    } else {
                        uart_read_bytes(EX_UART_NUM, dtmp, pos, 100 / portTICK_PERIOD_MS);
                        uint8_t pat[PATTERN_CHR_NUM + 1];
                        memset(pat, 0, sizeof(pat));
                        uart_read_bytes(EX_UART_NUM, pat, PATTERN_CHR_NUM, 100 / portTICK_PERIOD_MS);
                        ESP_LOGI(TAG, "read data: %s", dtmp);
                        ESP_LOGI(TAG, "read pat : %s", pat);
                    }
                    break;
                //Others
                default:
                    ESP_LOGI(TAG, "uart event type: %d", event.type);
                    break;
            }
        }
    }
    free(dtmp);
    dtmp = NULL;
    vTaskDelete(NULL);
}

void ESP_Recv_Data (uint8_t ID, uint8_t* data_recv)
{
    Data_Uart_onoff_t *Data_Uart_onoff ;
   model_scene_onoff_data_t * model_scene_onoff_data ;
    model_control_data_t  *model_control_data;
    model_delete_scene__t *model_delete_scene ;
    switch (ID)
    {
    case GEN_ONOFF_ID:
        printf("vao GEN_ONOFF_ID "); 
        Data_Uart_onoff = (Data_Uart_onoff_t *)data_recv ;
        ESP_LOGI(TAG, "data_adrr : 0x%04x, data_state: 0%02x \n",Data_Uart_onoff->addr, Data_Uart_onoff->state );
        example_ble_mesh_send_gen_onoff_set(Data_Uart_onoff->addr, Data_Uart_onoff->state);
        break;
    case SET_SCENE_ID:
     
     
     model_scene_onoff_data = (model_scene_onoff_data_t *)data_recv;
     handle_client_scene_set_store(model_scene_onoff_data->scene);
     ble_mesh_custom_scene_onoff_message_set(model_scene_onoff_data);
        
        break;
    case SET_CTRL_SCENE_ID :
      
      model_control_data =(model_control_data_t *)data_recv ;
      ble_mesh_custom_elm_control_client_model_message_set(model_control_data, model_control_data->element_control_adrr ) ;
        break;
    case DELETE_SCENE_ID :
        model_delete_scene = (model_delete_scene__t *)data_recv ;
        handle_client_scene_delete(model_delete_scene->scene_delete);
        
    break;
    default:
        break;
    }
}
void Uart_Gw_init (void)
{
    esp_log_level_set(TAG, ESP_LOG_INFO);

    /* Configure parameters of an UART driver,
     * communication pins and install the driver */
    uart_config_t uart_config = {
        .baud_rate = 115200,
        .data_bits = UART_DATA_8_BITS,
        .parity = UART_PARITY_DISABLE,
        .stop_bits = UART_STOP_BITS_1,
        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
        .source_clk = UART_SCLK_APB,
    };

    uart_driver_install(EX_UART_NUM, BUF_SIZE * 2, BUF_SIZE * 2, 20, &uart0_queue, 0);
    uart_param_config(EX_UART_NUM, &uart_config);


    esp_log_level_set(TAG, ESP_LOG_INFO);
 
    uart_set_pin(EX_UART_NUM, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE);

   
    uart_enable_pattern_det_baud_intr(EX_UART_NUM, '+', PATTERN_CHR_NUM, 9, 0, 0);

    uart_pattern_queue_reset(EX_UART_NUM, 20);

    xTaskCreate(uart_event_task, "uart_event_task", 2048, NULL, 12, NULL);
}

void Uart_resp_state (uint16_t addr, uint8_t state)
{       Data_Uart_onoff_t  state_onoff ;
        state_onoff.addr = addr;
        state_onoff.state =state;
        uint8_t * buff_send;
        buff_send = (uint8_t *)& (state_onoff.addr) ;
      uart_write_bytes(EX_UART_NUM, (uint8_t *)buff_send , sizeof(state_onoff));
}

void Uart_send_Addr_Note (uint16_t addr, uint8_t element_count)
{ 
    model_Send_note_infor_t note_infor ;
    note_infor.addr =addr;
    note_infor.element_count =element_count ;
    uint8_t *buff_send ;
    buff_send =(uint8_t *)&(note_infor.addr);
     uart_write_bytes(EX_UART_NUM,(uint8_t *)buff_send , 3);
   
}