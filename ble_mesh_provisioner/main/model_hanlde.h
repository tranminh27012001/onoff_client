#ifndef _MODEL_HANDLE_
#define _MODEL_HANDLE_

#include "esp_err.h"



/* HANDLE OF GENERIC ONOFF CLIENT*/
void example_ble_mesh_send_gen_onoff_set(uint16_t addr, uint8_t state);
void example_ble_mesh_send_gen_onoff_get(uint16_t addr);

/*HANDLE OF SCENE MODEL*/
void handle_client_scene_set_store( uint16_t scene_number_) ;
void handle_client_scene_delete( uint16_t scene_number_ ) ;
void handle_client_scene_set_ReCall ( uint16_t scene_number_, uint16_t net_idx, uint16_t app_idx) ;

/*custom handle*/
esp_err_t ble_mesh_custom_elm_control_client_model_message_set(model_control_data_t *set_data, uint16_t addr);
esp_err_t ble_mesh_custom_scene_onoff_message_set(model_scene_onoff_data_t *set_data) ;

// void set_scene_and_elm_control(void) ;
#endif