/* main.c - Application main entry point */

/*
 * Copyright (c) 2018 Espressif Systems (Shanghai) PTE LTD
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <stdio.h>
#include <string.h>

#include "esp_log.h"
#include "nvs_flash.h"
  
#include "esp_ble_mesh_defs.h"
#include "esp_ble_mesh_common_api.h"
#include "esp_ble_mesh_provisioning_api.h"
#include "esp_ble_mesh_networking_api.h"
#include "esp_ble_mesh_config_model_api.h"
#include "esp_ble_mesh_generic_model_api.h"
#include "esp_ble_mesh_local_data_operation_api.h"

#include "ble_mesh_example_init.h"

#include "custom_model_defs.c"
#include "model_handle.c"
#include "prov_handle.h"
#include "provisioner_gw.h"

#include "uart_gw.h"



void app_main(void)
{
    esp_err_t err;

    ESP_LOGI(TAG, "Initializing...");

    err = nvs_flash_init();
    if (err == ESP_ERR_NVS_NO_FREE_PAGES) {
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
    }
    ESP_ERROR_CHECK(err);

    err = ble_mesh_init();
    if (err) {
        ESP_LOGE(TAG, "Bluetooth mesh init failed (err %d)", err);
    }
   Uart_Gw_init();

//    set_scene_and_elm_control();
//    while (1)
//    {
    //  example_ble_mesh_send_gen_onoff_set(0x0006, 0x01);
//     vTaskDelay(1000/portTICK_RATE_MS);
//     example_ble_mesh_send_gen_onoff_set(0x0006, 0x00);
//     vTaskDelay(1000/portTICK_RATE_MS);
  
//    }

    //  vTaskDelay(10000/portTICK_RATE_MS);
    // set_scene_and_elm_control();


}
