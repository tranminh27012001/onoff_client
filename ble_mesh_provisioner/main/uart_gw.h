#ifndef _UART_GW_
#define _UART_GW_

#include <stdint.h>

#define BYTE_START 0x1F
#define BYTE_STOP 0xEF

#define GEN_ONOFF_ID 0x01
#define SET_SCENE_ID 0x02
#define SET_CTRL_SCENE_ID 0x03
#define DELETE_SCENE_ID 0x04
#define SET_PROVIDE 0x05

typedef struct 
{
   uint16_t addr;
   uint8_t state;
   
}Data_Uart_onoff_t;

typedef struct 
{
  uint16_t scene_delete ;
}model_delete_scene__t;

typedef struct 
{
   uint16_t addr ;
   uint8_t element_count;
}model_Send_note_infor_t;


 
 void Uart_Gw_init (void) ;
 void Uart_resp_state (uint16_t addr, uint8_t state) ;
void Uart_send_Addr_Note (uint16_t addr, uint8_t element_count);
#endif