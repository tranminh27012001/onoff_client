#ifndef _PROV_HANDLE_
#define _PROV_HANDLE_

#include "esp_ble_mesh_defs.h"

void recv_unprov_adv_pkt(uint8_t dev_uuid[16], uint8_t addr[BD_ADDR_LEN],
                                esp_ble_mesh_addr_type_t addr_type, uint16_t oob_info,
                                uint8_t adv_type, esp_ble_mesh_prov_bearer_t bearer);
void prov_link_open(esp_ble_mesh_prov_bearer_t bearer) ;
void prov_link_close(esp_ble_mesh_prov_bearer_t bearer, uint8_t reason) ;
esp_err_t prov_complete(int node_idx, const esp_ble_mesh_octet16_t uuid,
                               uint16_t unicast, uint8_t elem_num, uint16_t net_idx);
esp_ble_mesh_node_info_t *example_ble_mesh_get_node_info(uint16_t unicast);
 esp_err_t example_ble_mesh_set_msg_common(esp_ble_mesh_client_common_param_t *common,
                                                 esp_ble_mesh_node_info_t *node,
                                                 esp_ble_mesh_model_t *model, uint32_t opcode);
esp_err_t Add_group_address(uint16_t model_id, uint16_t group_addr);
void Gw_set_Group_Pub (uint16_t addr_note, uint16_t element_addr);
#endif